-- msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
-- Copyright (C) 2018  Matthias Meß
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;


 -- REG8_AVALON_INTERFACE is 8 bit memory mapped io which contains:
 --
 -- read:
 -- bit 0: ready (1 means ready)
 -- bit 1: timeout_err (1 means error)
 -- bit 2: checksum_err (1 means error)
 -- write:
 -- bit 7 is the enable signal.
 -- 
 -- Means bit 0 to 2 is written internal using q_export_write,
 --       bit 7      is written external by software using writedata
entity reg8_avalon_interface is
    port (
        clk, reset_n: in std_logic;
        read, write: in std_logic;
        writedata: in std_logic_vector(7 downto 0);
        readdata: out  std_logic_vector(7 downto 0);
        q_export_read: out  std_logic_vector(7 downto 0);
        q_export_write: in  std_logic_vector(7 downto 0)
    );
end reg8_avalon_interface;

architecture structure of reg8_avalon_interface is

    signal from_reg, to_reg : std_logic_vector(7 downto 0);

    component reg8
        port (
            clk, reset_n: in std_logic;
            d: in std_logic_vector(7 downto 0);
            q: out std_logic_vector(7 downto 0)
        );
    end component;
begin

    process(clk)
    begin
        if rising_edge(clk) then
            if write = '1' then
                to_reg(7 downto 4) <= writedata(7 downto 4);
            end if;
            
            if read = '1' then
                readdata <= from_reg;
            end if;
        end if;
    end process;
    to_reg(3 downto 0) <= q_export_write(3 downto 0);
    
    reg_instance: reg8
        port map (
            clk,
            reset_n,
            to_reg,
            from_reg
        );
 
    q_export_read <= from_reg;

end structure;
