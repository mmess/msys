/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *          
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef HELPER_H
#define HELPER_H

#include "sys/alt_stdio.h"
#include "alt_types.h"

#define DEBUG 1

#define dbg(...) \
    do{if(DEBUG)alt_printf(__VA_ARGS__);}while(0)

#define UNUSED(x) (void)(x)

#define getbit(mask, x) ((mask >> x) & 1U)
#define setbit(mask, x) (mask |= 1UL << x)
#define clrbit(mask, x) (mask &= ~(1UL << x))
#define togglebit(mask, x) (mask ^= 1UL << x)

void printbits(unsigned int const size, void const * const ptr);
void dbg_hpi_status(alt_u16 status);

#endif

