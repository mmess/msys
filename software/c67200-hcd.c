/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Matthias Mess
 *
 * USB Host Controller Driver for CYC67200 on Altera FPGA
 *
 * The driver implements the lowlevel HPI interface to the USB-Controller
 * Reset/Read/Write/Interrupt handling.
 */
#include <stdlib.h>
#include "sys/alt_stdio.h"
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <system.h>
#include <limits.h>
#include "c67200-hcd.h"
#include "helper.h"
#include <io.h>

#define COMM_REGS 14
/* lcp: link control protocol*/
struct lcp_int_data {
    alt_u16 regs[COMM_REGS];
};

/* Interface definitions */
#define COMM_ACK        0x0FED
#define COMM_NAK        0xDEAD
#define COMM_RESET      0xFA50
#define COMM_EXEC_INT   0xCE01
#define COMM_INT_NUM    0x01C2

/* Registers 0 to COMM_REGS-1 */
#define COMM_R(x)                   (0x01C4 + 2 * (x))

#define HUSB_SIE_pCurrentTDPtr(x)   ((x) ? 0x01B2 : 0x01B0)
#define HUSB_SIE_pTDListDone_Sem(x) ((x) ? 0x01B8 : 0x01B6)
#define HUSB_pEOT                   0x01B4
/* Software interrupts */
/* 114, 115: */
#define HUSB_SIE_INIT_INT(x)        ((x) ? 0x0073 : 0x0072)
#define HUSB_RESET_INT              0x0074
#define SUSB_INIT_INT               0x0071
#define SUSB_INIT_INT_LOC           (SUSB_INIT_INT * 2)

/* HPI registers */
#define HPI_DATA    0
#define HPI_MAILBOX 1
#define HPI_ADDR    2
#define HPI_STATUS  3

#define HPI_SWAP_ENABLE(x)	((x) ? 0x0100 : 0x0001)
#define RESET_TO_HPI_ENABLE(x)	((x) ? 0x0200 : 0x0002)
#define DONE_TO_HPI_ENABLE(x)	((x) ? 0x0008 : 0x0004)
#define RESUME_TO_HPI_ENABLE(x)	((x) ? 0x0080 : 0x0040)
#define SOFEOP_TO_HPI_EN(x)	((x) ? 0x2000 : 0x0800)
#define SOFEOP_TO_CPU_EN(x)	((x) ? 0x1000 : 0x0400)
#define ID_TO_HPI_ENABLE	0x4000
#define VBUS_TO_HPI_ENABLE	0x8000

/*
 *  According to CY7C67200 specification (page 74) HPI read and
 *  write cycle duration Tcyc must be at least 6T long, where T is 1/48MHz,
 *  which is 125ns. There is no nanosleep function available so I use
 *  usleep and sleep 1us.
 */
#define HPI_T_CYC_US 1

#define MSG_RECEIVED 1
#define CLEAR_MSG_RECEIVED 0

// check if the D+ status bit of Port A is set.
// parameter is the USB n control register (page 20)
#define DP_STAT 0x2000
#define DM_STAT 0x1000
#define dev_sie1_connected(x) ((x) & (DP_STAT | DM_STAT))
#define DEFAULT_EOT 600

static alt_u16 hpi_read_reg(struct usb_device *dev, int reg)
{
    usleep(HPI_T_CYC_US);
    return IORD(dev->hpi.base, reg);
}


static void hpi_write_reg(struct usb_device *dev, int reg, alt_u16 value)
{
    usleep(HPI_T_CYC_US);
    //dbg("HPI write reg: addr:%x, data:%x\n", reg, value);
    IOWR(dev->hpi.base, reg, value);
}


/**
 * This function writes data to the internal registers of the Cypress
 * CY7C67200 USB controller.
 *
 * @param    reg is the address of the register.
 * @param    value is the data to be written to the register.
 *
 */
static void hpi_write_word(struct usb_device *dev, alt_u16 reg, alt_u16 value)
{
    //dbg("hpi_write_word: HPI_ADDR: %x : HPI_DATA:%x\n", reg, value);
    IOWR(dev->hpi.base, HPI_ADDR, reg);
    IOWR(dev->hpi.base, HPI_DATA, value);
}


/**
 * This function reads data from the internal registers of the Cypress
 * CY7C67200 USB controller.
 *
 * @param reg is the address of the register.
 */
alt_u16 hpi_read_word(struct usb_device *dev, alt_u16 reg)
{
    alt_u16 res;
    hpi_write_reg(dev, HPI_ADDR, reg);
    res = hpi_read_reg(dev, HPI_DATA);
    return res;
}


/*
 * Only data is little endian, addr has cpu endianess
 * Nios 2 is also le, we dont handle different cpus for now.
 */
static void hpi_write_words(struct usb_device *dev, alt_u16 addr,
    alt_u16 *data, alt_u16 count)
{
    int i;
    alt_u16 tmp;
    hpi_write_reg(dev, HPI_ADDR, addr);
    for (i=0; i<count; i++) {
        tmp = *data++;
        hpi_write_reg(dev, HPI_DATA, tmp);
    }
}


/*
 * Only data is little endian, addr has cpu endianess
 * Nios 2 is also le, we dont handle different cpus for now.
 */
static void hpi_read_words(struct usb_device *dev, alt_u16 addr,
    alt_u16 *data, alt_u16 count)
{
    int i;
    hpi_write_reg(dev, HPI_ADDR, addr);
    for (i = 0; i < count; i++) {
        *data++ = hpi_read_reg(dev, HPI_DATA);
    }
}


/**
 * write_mem - write into c67200 memory
 * Only data is little endian, addr has cpu endianess.
 */
void write_mem(struct usb_device *dev, alt_u16 addr, void *data, int len)
{
    alt_u8 *buf = data;
    /* Sanity check */
    if (addr + len > 0xffff) {
        alt_printf( "Trying to write beyond writable region!\n");
        return;
    }

    if (addr & 0x01) {
        /* unaligned access */
        dbg("unaligned access\n");
        alt_u16 tmp;
        tmp = hpi_read_word(dev, addr - 1);
        tmp = (tmp & 0x00ff) | (*buf++ << 8);
        hpi_write_word(dev, addr - 1, tmp);
        addr++;
        len--;
    }

    hpi_write_words(dev, addr, (alt_u16 *)buf, len / 2);
    buf += len & ~0x01;
    addr += len & ~0x01;
    len &= 0x01;
    if (len) {
        alt_u16 tmp;
        tmp = hpi_read_word(dev, addr);
        tmp = (tmp & 0xff00) | *buf;
        hpi_write_word(dev, addr, tmp);
    }
}


/**
 * reads from c67200 memory
 */
void read_mem(struct usb_device *dev, alt_u16 addr, void *data, int len)
{
    alt_u8 *buf = data;

    if (addr & 0x01) {
        /* unaligned access */
        alt_u16 tmp;
        tmp = hpi_read_word(dev, addr - 1);
        *buf++ = (tmp >> 8) & 0x00ff;
        addr++;
        len--;
    }

    hpi_read_words(dev, addr, (alt_u16 *)buf, len / 2);

    buf += len & ~0x01;
    addr += len & ~0x01;
    len &= 0x01;
    if (len) {
        alt_u16 tmp;
        tmp = hpi_read_word(dev, addr);
        *buf = tmp & 0x00ff;
    }
}


/**
 * Set bits of a HPI register bitmask
 */
static void hpi_set_bits(struct usb_device *dev, alt_u16 reg, alt_u16 mask)
{
    alt_u16 value;
    value = hpi_read_word(dev, reg);
    hpi_write_word(dev, reg, value | mask);
}


/**
 * Clear bits of a HPI register bitmask
 */
static void hpi_clear_bits(struct usb_device *dev, alt_u16 reg, alt_u16 mask)
{
    alt_u16 value;
    value = hpi_read_word(dev, reg);
    hpi_write_word(dev, reg, value & ~mask);
}


static alt_u16 hpi_recv_mbox(struct usb_device *dev)
{
    alt_u16 value;

    value = hpi_read_reg(dev, HPI_MAILBOX);
    return value;
}


static alt_u16 hpi_send_mbox(struct usb_device *dev, alt_u16 value)
{
    dbg("Write to mailbox: 0x%x\n", value);
    hpi_write_reg(dev, HPI_MAILBOX, value);
    return value;
}


alt_u16 hpi_status(struct usb_device *dev)
{
    return hpi_read_reg(dev, HPI_STATUS);
}


void hpi_reg_init(struct usb_device *dev)
{
    int i;
    hpi_recv_mbox(dev);
    hpi_status(dev);

    // Had to add the write due to a bug in BIOS where they overwrite
    // the mailbox after initialization with garbage.  The read clears
    // any pending interrupts.

    for (i = 0; i < C67X00_SIES; i++) {
        hpi_read_word(dev, SIEMSG_REG(i));
        dbg("write SIEMsg%x HPI ADDR:%x, HPI DATA: %x\n", i, SIEMSG_REG(i), 0);
        hpi_write_word(dev, SIEMSG_REG(i), 0);
        dbg("write Host%x Status Reg HPI ADDR:%x, HPI DATA: %x\n",
            i, USB_STAT_REG(i), 0xffff);
        hpi_write_word(dev, USB_STAT_REG(i), 0xffff);
    }
}


void hpi_enable_sofeop(struct sie *sie)
{
    hpi_set_bits(sie->dev, HPI_IRQ_ROUTING_REG, SOFEOP_TO_HPI_EN(sie->num));
}


void hpi_disable_sofeop(struct sie *sie)
{
    dbg("disable sofeop\n");
    hpi_clear_bits(sie->dev, HPI_IRQ_ROUTING_REG, SOFEOP_TO_HPI_EN(sie->num));
}


/**
 * Waits until an interrupt tells that an HPI LCP message was received.
 */
static int recv_msg(struct usb_device *dev)
{
    int int_status = hpi_status(dev);

    while(!int_status) {

		usleep(1000);
        int_status = hpi_status(dev);
    }

    dbg_hpi_status(int_status);

    return 1;
}


alt_u16 fetch_siemsg(struct usb_device *dev, int sie_num)
{
    alt_u16 val;
    val = hpi_read_word(dev, SIEMSG_REG(sie_num));
    /* clear register to allow next message */
    if(val != 0) {
        hpi_write_word(dev, SIEMSG_REG(sie_num), 0);
    }
    return val;
}


alt_u16 get_usb_ctl(struct sie *sie)
{
    return hpi_read_word(sie->dev, USB_CTL_REG(sie->num));
}


void set_usb_ctl(struct sie *sie, alt_u16 data)
{
    hpi_write_word(sie->dev, USB_CTL_REG(sie->num), data);
}


/**
 * Clear the USB status bits.
 */
void usb_clear_status(struct sie *sie, alt_u16 bits)
{
    hpi_write_word(sie->dev, USB_STAT_REG(sie->num), bits);
}


alt_u16 usb_get_status(struct sie *sie)
{
    return hpi_read_word(sie->dev, USB_STAT_REG(sie->num));
}


static int comm_exec_int(struct usb_device *dev, alt_u16 nr,
    struct lcp_int_data *data)
{
    int i, rc;
    dbg("exec int\n");
    hpi_write_word(dev, COMM_INT_NUM, nr);
    dbg("write registers\n");

    for (i = 0; i < COMM_REGS; i++) {
        hpi_write_word(dev, COMM_R(i), data->regs[i]);
    }

    hpi_send_mbox(dev, COMM_EXEC_INT);
    rc = recv_msg(dev);
    dbg("exec INT with status: %x\n", rc);

    return rc;
}


/**
 * set end of transfer.
 */
void set_husb_eot(struct usb_device *dev, alt_u16 value)
{
    dbg("HUSB_pEOT(%x): %x\n", HUSB_pEOT, value);
    hpi_write_word(dev, HUSB_pEOT, value);
}


static void husb_sie_init(struct sie *sie)
{
    int rc;
    struct usb_device *dev = sie->dev;
    struct lcp_int_data data;

    for(int i=0; i<COMM_REGS; i++) {
        data.regs[i] = 0;
    }

    husb_reset(&dev->sie[0], 0);
    dbg("HUSB_SIE_INIT\n");
    rc = comm_exec_int(dev, HUSB_SIE_INIT_INT(sie->num), &data);
    assert(rc);
}


void husb_reset(struct sie *sie, int port)
{
    struct usb_device *dev = sie->dev;
    struct lcp_int_data data;
    int rc;
    alt_printf("husb_reset() - HUSB_RESET_INT\n");
    data.regs[0] = 0x003C;  /* Reset USB port for 50ms */
    data.regs[1] = port | (sie->num << 1);
    for(int i=2; i<COMM_REGS; i++) {
        data.regs[i] = 0;
    }
    rc = comm_exec_int(dev, HUSB_RESET_INT, &data);

    if(!rc) {
        alt_printf("err: husb_reset %x\n", rc);
    }

    assert(rc);

    // clear connect change
    usb_clear_status(sie, PORT_CONNECT_CHANGE(port));

	/* Enable interrupts */
    dbg("write IRQ routing reg:%x, value:%x\n", HPI_IRQ_ROUTING_REG,
        SOFEOP_TO_CPU_EN(sie->num) | RESUME_TO_HPI_ENABLE(sie->num));
    dbg("write HOST IRQ EN reg:%x, value:%x\n", HOST_IRQ_EN_REG(sie->num),
        SOF_EOP_IRQ_EN | DONE_IRQ_EN);
	hpi_set_bits(sie->dev, HPI_IRQ_ROUTING_REG,
		     SOFEOP_TO_CPU_EN(sie->num) | RESUME_TO_HPI_ENABLE(sie->num));
	hpi_set_bits(sie->dev, HOST_IRQ_EN_REG(sie->num),
		     SOF_EOP_IRQ_EN | DONE_IRQ_EN);
	/* Enable pull down transistors */
	hpi_set_bits(sie->dev, USB_CTL_REG(sie->num), PORT_RES_EN(port));

    husb_reset_port(sie, port);
}


void husb_set_current_td(struct sie *sie, alt_u16 addr)
{
    hpi_write_word(sie->dev, HUSB_SIE_pCurrentTDPtr(sie->num), addr);
}


alt_u16 husb_get_current_td(struct sie *sie)
{
    alt_u16 val = hpi_read_word(sie->dev, HUSB_SIE_pCurrentTDPtr(sie->num));
    dbg("check current td: 0x%x\n", val);
    return val;
}


void husb_init_host_port(struct sie *sie)
{
    dbg("init host port\n");
    /* Set port into host mode */
    hpi_set_bits(sie->dev, USB_CTL_REG(sie->num), HOST_MODE);
    husb_sie_init(sie);
    /* Clear interrupts */
    usb_clear_status(sie, HOST_STAT_MASK);
    /* Check */
    if (!(hpi_read_word(sie->dev, USB_CTL_REG(sie->num)) & HOST_MODE)) {
        alt_printf("SIE %x not set to host mode\n", sie->num);
    }
}


void husb_reset_port(struct sie *sie, int port)
{
    /* Clear connect change */
    usb_clear_status(sie, PORT_CONNECT_CHANGE(port));

    /* Enable interrupts */
    hpi_set_bits(sie->dev, HPI_IRQ_ROUTING_REG, SOFEOP_TO_CPU_EN(sie->num));
    hpi_set_bits(sie->dev, HOST_IRQ_EN_REG(sie->num), SOF_EOP_IRQ_EN | DONE_IRQ_EN);

    /* Enable pull down transistors */
    hpi_set_bits(sie->dev, USB_CTL_REG(sie->num), PORT_RES_EN(port));
}


int hcd_next_tag(struct hcd *hcd)
{
    int tag = hcd->tag;
    if(hcd->tag == INT_MAX) {
        hcd->tag = 1;
    } else {
        hcd->tag += 1;
    }

    return tag;
}


void init_device(struct usb_device *dev)
{
    dbg("init device\n");
    struct platform_data *pdata = malloc(sizeof(struct platform_data));

    if(pdata == NULL) {
        errno = ENOMEM;
        alt_printf("err on malloc\n");
        exit(1);
    }

    dev->hpi.base = CY7C67200_IF_0_BASE;
    dev->hpi.lcp.msg_received = CLEAR_MSG_RECEIVED;
    dev->pdata = pdata;
    dev->devnum = 1;
    dev->dev_descr = malloc(sizeof(struct device_descriptor));
    dev->cfg_descr = malloc(sizeof(struct config_descriptor));
    dev->if_descr = malloc(sizeof(struct interface_descriptor));
    dev->ep_descr[0] = malloc(sizeof(struct endpoint_descriptor));
    dev->ep_descr[1] = malloc(sizeof(struct endpoint_descriptor));

    if(dev->dev_descr == NULL || dev->cfg_descr == NULL ||
        dev->if_descr == NULL || dev->ep_descr[0] == NULL ||
        dev->ep_descr[1] == NULL) {
        alt_printf("memory allocation failed\n");
        exit(-ENOMEM);
    }

    hpi_reg_init(dev);
}


int reset_device(struct usb_device *dev)
{
    int rc;
    hpi_send_mbox(dev, COMM_RESET);
    rc = recv_msg(dev);
    // looks like this must be done before husb init
    // have to see if it must be done again here.
    //husb_reset(&dev->sie[0], port);
    return rc;
}


static void init_hcd(struct sie *sie)
{
    struct hcd *hcd = malloc(sizeof(struct hcd));
    dbg("Init hcd\n");

    if(hcd == NULL) {
        errno = ENOMEM;
        alt_printf("err on malloc\n");
        exit(1);
    }
    hcd->td_base_addr = CY_HCD_BUF_ADDR + SIE_TD_OFFSET(sie->num);
    hcd->buf_base_addr = CY_HCD_BUF_ADDR + SIE_BUF_OFFSET(sie->num);
    hcd->sie = sie;
    hcd->toggle = 1;
    hcd->tag = 1;
    sie->hcd = hcd;

    husb_init_host_port(sie);
    hcd->endpoint_disable = 1;
}


void init_sie(struct sie *sie, struct usb_device *dev, int sie_num)
{
    sie->dev = dev;
    sie->num = sie_num;
    sie->mode = sie_config(dev->pdata->sie_config, sie_num);
    sie->irq = NULL;

    switch (sie->mode) {
        case C67X00_SIE_HOST:
            alt_printf("Init SIE %x as requested\n", sie->num);
            init_hcd(sie);
            break;
        case C67X00_SIE_UNUSED:
            alt_printf("Not using SIE %x as requested\n", sie->num);
            break;
        default:
            alt_printf("Unsupported configuration: 0x%x for SIE %d\n",
                sie->mode, sie->num);
            break;
    }
}


void wait_for_connect(struct sie *sie)
{
    alt_u16 status = get_usb_ctl(sie);
    dbg("wait for device connect\n");
    while(!dev_sie1_connected(status)){
        usleep(10000);
        status = get_usb_ctl(sie);
    }
    dbg("device connected\n");
}


void init(struct usb_device *dev, int sie_num, int port) {

    reset_device(dev);
    hpi_reg_init(dev);

    set_husb_eot(dev, DEFAULT_EOT);
    husb_reset(&dev->sie[sie_num], port);
}

