/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sys/alt_stdio.h"
#include "helper.h"

void printbits(unsigned int const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1; i>=0; i--) {
        for (j=7;j>=0;j--) {
            byte = (b[i] >> j) & 1;
            alt_printf("%u", byte);
        }
    }
    alt_putstr("");
}

void dbg_hpi_status(alt_u16 status)
{
    alt_printf("HPI Status 0x%x\n", status);
    if(status & 0x0001) {
        alt_printf("bit 0 set: msg ready in outgoing mailbox.\n");
    }
    if(status & 0x0002) {
        alt_printf("bit 1 set: usb reset irq occured.\n");
    }
    if(status & 0x0004) {
        alt_printf("bit 2 set: host 1 pkt done irq occured.\n");
    }
    if(status & 0x0008) {
        alt_printf("bit 3 set: host 2 pkt done irq occured.\n");
    }
    if(status & 0x0010) {
        alt_printf("bit 4 set: CY7C67200 CPU has written to the SIE1msg register.\n");
    }
    if(status & 0x0020) {
        alt_printf("bit 5 set: CY7C67200 CPU has written to the SIE2msg register.\n");
    }
    if(status & 0x0040) {
        alt_printf("bit 6 set: resume irq on host 1.\n");
    }
    if(status & 0x0080) {
        alt_printf("bit 7 set: resume irq on host 2.\n");
    }
    if(status & 0x0100) {
        alt_printf("bit 8 set: message is ready in the incoming mailbox..\n");
    }
    if(status & 0x0200) {
        alt_printf("bit 9 set: Reset interrupt occurs on either Host/Device 2.\n");
    }
    if(status & 0x0400) {
        alt_printf("bit 10 set: SOF/EOP interrupt occurs on either Host/Device 1.\n");
    }
    // bit 11 is reserved, so we dont check it
    if(status & 0x1000) {
        alt_printf("bit 12 set: SOF/EOP interrupt occurs on either Host/Device 2.\n");
    }
    // bit 13 is reserved, so we dont check it
    if(status & 0x4000) {
        alt_printf("bit 14 set: ID Flag OTG ID pin set.\n");
    }
    if(status & 0x8000) {
        alt_printf("bit 15 set: OTG VBus is greater then 4.4 V.\n");
    }
}

