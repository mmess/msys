/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Matthias Meß
 *
 * USB Bulk Transfer Driver for CYC67200 on Altera FPGA
 *
 * The driver implements the control and bulk requests needed to
 * write data to a msc device.
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include "c67200-hcd.h"
#include "c67200-bulk.h"
#include "sys/alt_stdio.h"
#include "scsi.h"
#include "helper.h"

#define SETUP_STAGE     0
#define DATA_STAGE      1
#define STATUS_STAGE    2

#define USB_ENDPOINT_MAXP_MASK  0x07ff

#define DESCR_TYPE_DEV 1
#define DESCR_TYPE_CFG 2
#define DESCR_TYPE_STR 3
#define DESCR_TYPE_IF 4
#define DESCR_TYPE_EP 5

/* USB Control Transfer standard requests */
#define CTRL_GET_STATUS        0x00
#define CTRL_CLEAR_FEATURE     0x01
#define CTRL_SET_FEATURE       0x03
#define CTRL_SET_ADDRESS       0x05
#define CTRL_GET_DESCRIPTOR    0x06
#define CTRL_SET_DESCRIPTOR    0x07
#define CTRL_GET_CONFIGURATION 0x08
#define CTRL_SET_CONFIGURATION 0x09
/* USB Endpoint Feature */
#define ENDPOINT_HALT 0x00

/**
 * Device holds 2 endpoint descriptors. One is In the other is OUT.
 * The IN descriptor has a endpoint address of 0x80.
 */
#define bulk_in_ep_descr(dev) \
    ((dev->ep_descr[0]->bEndpointAddress | 0x80) \
    ? dev->ep_descr[0] \
    : dev->ep_descr[1])

#define bulk_out_ep_descr(dev) \
    ((dev->ep_descr[0]->bEndpointAddress | 0x80) \
    ? dev->ep_descr[1] \
    : dev->ep_descr[0])

#define bulk_in_ep_addr(dev) \
    ((dev->ep_descr[0]->bEndpointAddress | 0x80) \
    ? dev->ep_descr[0]->bEndpointAddress \
    : dev->ep_descr[1]->bEndpointAddress)

#define bulk_out_ep_addr(dev) \
    ((dev->ep_descr[0]->bEndpointAddress | 0x80) \
    ? dev->ep_descr[1]->bEndpointAddress \
    : dev->ep_descr[0]->bEndpointAddress)

/**
 * Transfer Descriptor
 * struct td
 *
 * Hardware parts are little endiannes, SW in CPU endianess.
 */
struct td {
    /* HW specific part */
    alt_u16 ly_base_addr;   /* Bytes 0-1 base addr of data buffer */
    /* The TD data length (0-9) and port number (14-15) */
    alt_u16 port_length;    /* Bytes 2-3 */
    /* The PID and Endpoint number, PID (7-4), EP (3-0)
     * see bios manual 3.5.3
     **/
    alt_u8 pid_ep;          /* Byte 4 */
    alt_u8 dev_addr;        /* Byte 5 */
    alt_u8 ctrl_reg;        /* Byte 6 */
    alt_u8 status;          /* Byte 7 */
    alt_u8 retry_cnt;       /* Byte 8 */
    alt_u8 residue;         /* Byte 9 */
    alt_u16 next_td_addr;   /* Bytes 10-11, ptr to next td */
    /* SW part */
    struct td *nxt_td; /* internal used for linked list behavior */
    alt_u16 td_addr;
    alt_u16 buf_addr;
    void *data;
    struct pipe pipe;
};

#define CY_TD_SIZE          12
#define TD_PIDEP_OFFSET     0x04
#define TD_PIDEPMASK_PID    0xF0
#define TD_PIDEPMASK_EP     0x0F
#define TD_PORTLENMASK_DL   0x03FF
#define TD_PORTLENMASK_PN   0xC000
#define TD_STATUS_OFFSET    0x07
#define TD_STATUSMASK_ACK   0x01
#define TD_STATUSMASK_ERR   0x02
#define TD_STATUSMASK_TMOUT 0x04
#define TD_STATUSMASK_SEQ   0x08
#define TD_STATUSMASK_SETUP 0x10
#define TD_STATUSMASK_OVF   0x20
#define TD_STATUSMASK_NAK   0x40
#define TD_STATUSMASK_STALL 0x80
#define TD_ERROR_MASK       (TD_STATUSMASK_ERR | TD_STATUSMASK_TMOUT | \
                 TD_STATUSMASK_STALL)
#define TD_RETRYCNT_OFFSET      0x08
#define TD_RETRYCNTMASK_ACT_FLG 0x10
#define TD_RETRYCNTMASK_TX_TYPE 0x0C
#define TD_RETRYCNTMASK_RTY_CNT 0x03
#define TD_RESIDUE_OVERFLOW     0x80
#define TD_PID_IN               0x90

/* Residue: signed 8bits, neg -> OVERFLOW, pos -> UNDERFLOW */
#define td_residue(td)      ((signed char)(td->residue))
#define td_active(td)       (td->retry_cnt & TD_RETRYCNTMASK_ACT_FLG)
#define td_length(td)       (td->port_length & TD_PORTLENMASK_DL)
#define td_sequence_ok(td)  (!td->status || \
    (!(td->status & TD_STATUSMASK_SEQ) == !(td->ctrl_reg & SEQ_SEL)))
#define td_acked(td) (!td->status || (td->status & TD_STATUSMASK_ACK))
#define td_actual_bytes(td) (td_length(td) - td_residue(td))

/**
 * dbg_td - Dump the contents of the TD
 */
static void dbg_td(struct td *td, char *msg)
{
    alt_printf("### %s at     0x%x\n", msg, td->td_addr);
    alt_printf("ly_base_addr: 0x%x\n", td->ly_base_addr);
    alt_printf("port_length:  0x%x\n", td->port_length);
    alt_printf("pid_ep:       0x%x\n", td->pid_ep);
    alt_printf("dev_addr:     0x%x\n", td->dev_addr);
    alt_printf("ctrl_reg:     0x%x\n", td->ctrl_reg);
    alt_printf("status:       0x%x\n", td->status);
    alt_printf("retry_cnt:    0x%x\n", td->retry_cnt);
    alt_printf("residue:      0x%x\n", td->residue);
    alt_printf("next_td_addr: 0x%x\n", td->next_td_addr);
}

/**
 * dbg_cbw - Dump the contents of the Command control block
 */
static void dbg_cbw(struct cbw *cbw)
{
    alt_printf("Signature:    0x%x\n", cbw->Signature);
    alt_printf("Tag:          0x%x\n", cbw->Tag);
    alt_printf("DataTransferLength 0x%x\n", cbw->DataTransferLength);
    alt_printf("Flags:        0x%x\n", cbw->Flags);
    alt_printf("Lun:          0x%x\n", cbw->Lun);
    alt_printf("Length:       0x%x\n", cbw->Length);
    alt_printf("CDB: ");
    for(int i=0;i<cbw->Length; ++i) {
        alt_printf("%x ", cbw->CDB[i]);
    }
    alt_printf("\n");
}


static void dbg_csw(struct csw *csw)
{
    alt_printf("\nCSW:\n");
    alt_printf("Signature: 0x%x\n", csw->Signature);
    alt_printf("Tag:       0x%x\n", csw->Tag);
    alt_printf("Residue:   0x%x\n", csw->Residue);
    alt_printf("Status:    0x%x\n", csw->Status);
}

/**
 * Free the whole TD List and also regardind data.
 */
static void release_td(struct td *td)
{
    int len;
    struct td *nxt_td = td;
    dbg("Release TDList\n");
    do {
        td = nxt_td;
        len = td_length(td);

        if (len && ((td->pid_ep & TD_PIDEPMASK_PID) != TD_PID_IN)) {
            free(td->data);
        }
        nxt_td = td->nxt_td;
        free(td);
    } while(nxt_td != NULL);
}

/**
 * Get TD from C67200
 */
static void parse_td(struct hcd *hcd, struct td *td)
{
	read_mem(hcd->sie->dev, td->td_addr, td, CY_TD_SIZE);

	if (td->pipe.direction == USB_DIR_IN && td_actual_bytes(td)) {
        dbg("read td->data\n");
		read_mem(hcd->sie->dev, td->ly_base_addr,
			td->data, td_actual_bytes(td));
    }
}


static void parse_descr(struct hcd *hcd, void *buf, int len, alt_u16 addr)
{
    dbg("read descr len %x from addr: 0x%x\n", len, addr);
	read_mem(hcd->sie->dev, addr, buf, len);
}

/**
 * Send td to C67X00
 */
static void send_td(struct hcd *hcd, struct td *td)
{
    int len = td_length(td);
    dbg("send td with len 0x%x .. \n", len);

    if (len && ((td->pid_ep & TD_PIDEPMASK_PID) != TD_PID_IN)) {
        dbg("write td->data to 0x%x\n", td->ly_base_addr);
        write_mem(hcd->sie->dev, td->ly_base_addr, td->data, len);
    }

    dbg_td(td, "write td");
    write_mem(hcd->sie->dev, td->td_addr, td, CY_TD_SIZE);
}


void create_td(struct hcd *hcd, struct td *td, alt_u8 pid, int data_len,
    alt_u8 ep)
{
	alt_u8 cmd = 0;
    td->td_addr = hcd->next_td_addr;
    hcd->next_td_addr += CY_TD_SIZE;

    if(data_len > 0) {
        td->ly_base_addr = hcd->next_buf_addr;
    } else {
        td->ly_base_addr = 0;
    }

    td->port_length = 0x0000 | data_len; // assumes port is 0
    td->pid_ep = ((pid & 0xF) << TD_PIDEP_OFFSET) | (ep & 0xF);
    td->dev_addr = hcd->sie->dev_addr;

	if (hcd->toggle)
		cmd |= SEQ_SEL;

	cmd |= ARM_EN;

    td->ctrl_reg = cmd;
    td->status = 0x0;
    td->retry_cnt = 0x13;
    td->nxt_td = NULL;
    td->residue = 0x0;
    td->next_td_addr = hcd->next_td_addr;
    hcd->next_buf_addr += (data_len + 1) & ~0x01; /* properly align */
    td->nxt_td = NULL;

    td->pipe.num = 0;
    td->pipe.type = PIPE_CONTROL;
    if ((td->pid_ep & TD_PIDEPMASK_PID) == TD_PID_IN) {
        td->pipe.direction = USB_DIR_IN;
    } else {
        td->pipe.direction = USB_DIR_OUT;
    }
}


void create_command_td(struct hcd *hcd, struct td *td, alt_u8 pid)
{
    alt_u8 ep;
	alt_u8 cmd = 0;
    alt_u8 cbw_len = sizeof(struct cbw);
    td->td_addr = hcd->next_td_addr;
    hcd->next_td_addr += CY_TD_SIZE;
    td->ly_base_addr = hcd->next_buf_addr;
    td->port_length = 0x0000 | cbw_len; // assumes port is 0
    ep = bulk_out_ep_addr(hcd->sie->dev);

    // 4 bit pid and 4 bit ep
    td->pid_ep = ((pid & 0xF) << TD_PIDEP_OFFSET) | (ep & 0xF);
    td->dev_addr = hcd->sie->dev_addr;

	if (hcd->toggle)
		cmd |= SEQ_SEL;

	cmd |= ARM_EN;

    td->ctrl_reg = cmd;
    td->status = 0x0;
    // b0-1: retry cnt, b2-3 transfertype, b4 active flag, 5-7 reserved
    td->retry_cnt = 0x1B; // bulk
    td->residue = 0x0;
    td->next_td_addr = hcd->next_td_addr;
    td->nxt_td = NULL;
    hcd->next_buf_addr += (cbw_len + 1) & ~0x01; /* properly align */
    // TODO: verify if that is still needed.
    td->pipe.num = 1;
    td->pipe.type = PIPE_BULK;
    if ((td->pid_ep & TD_PIDEPMASK_PID) == TD_PID_IN) {
        td->pipe.direction = USB_DIR_IN;
    } else {
        td->pipe.direction = USB_DIR_OUT;
    }
}


void create_setup_td(struct hcd *hcd, struct td *td, alt_u8 pid)
{
    int data_len = 8;
    create_td(hcd, td, pid, data_len, 0);
    td->port_length = 0x0000 | data_len;
    td->ctrl_reg = 0x1;
}


void create_status_td(struct hcd *hcd, struct td *td, alt_u8 pid, alt_u8 ep)
{
    int data_len = 0;
    create_td(hcd, td, pid, data_len, ep);
    td->ctrl_reg = 0x41;
    td->next_td_addr = 0x0;
}


void create_data_td(struct hcd *hcd, struct td *td, alt_u8 pid, int data_len,
    alt_u8 ep)
{
    create_td(hcd, td, pid, data_len, ep);
    td->port_length = 0x0000 | data_len;
}


int wait_tddone(struct hcd *hcd)
{
    int cnt = 5;
    int ret = fetch_siemsg(hcd->sie->dev, hcd->sie->num);

    while(ret != HUSB_TDListDone) {
        usleep(100000);
        ret = fetch_siemsg(hcd->sie->dev, hcd->sie->num);
        if(cnt-- == 0) {
            dbg("err: SIE timed out.\n");
            return false;
        }
    }
    return true;
}


bool is_td_ok(struct hcd *hcd, struct td *td) {
    int retry = 3;
    parse_td(hcd, td);
    while(td_active(td))  {
        usleep(100000);
        parse_td(hcd, td);
        if(retry-- == 0) {
            alt_printf("TD timed out\n");
            return false;
        }
    }

    if(td->status & TD_ERROR_MASK || (td->status & TD_STATUSMASK_NAK)
        || !td_sequence_ok(td) || !td_acked(td)) {
        return false;
    }

    return true;
}


    /**** USB enumeration related functions ****/

/**
 * Create an USB GET_DESCRIPTOR control package
 */
void create_get_descriptor_pkt(struct usb_ctrlrequest* pkt, alt_u16 type,
    alt_u16 idx, alt_u16 len)
{
    pkt->bRequestType = USB_DIR_IN; /* direction from device to host (IN)*/
    pkt->bRequest = CTRL_GET_DESCRIPTOR;
    /* Descriptor Type (H) and Descriptor Index (L) */
    if(type == DESCR_TYPE_DEV) {
        pkt->wValue = (type << 8) | idx;
        pkt->wIndex = 0x0;
    } else {
        pkt->wValue = (DESCR_TYPE_CFG << 8) | idx;
        pkt->wIndex = 0x0; /* index of descr, if only one then not relevant */
    }
    /* length of the descr what should be retrieved */
    pkt->wLength = len;
}

/**
 * Create an USB SET_ADDRESS control package
 */
void create_addr_pkt(struct usb_ctrlrequest* pkt, alt_u16 addr)
{
    /**
     * D7:   0:host to device / 1:device to host
     * D6-5: 0: standard, 1: class, 2: vendor, 3: reserved
     */
    pkt->bRequestType = 0x00;
    pkt->bRequest = CTRL_SET_ADDRESS;
    pkt->wValue = addr; /* the device addr */
    pkt->wIndex = 0x0000; /* offset */
    pkt->wLength = 0x0000; /* data length */
}

/**
 * Create an USB SET_CONFIGURATION control package
 */
static void create_set_config_pkt(struct usb_ctrlrequest* pkt, alt_u8 cfg)
{
    /**
     * D7:   0:host to device / 1:device to host
     * D6-5: 0: standard, 1: class, 2: vendor, 3: reserved
     */
    pkt->bRequestType = 0x00;
    pkt->bRequest = CTRL_SET_CONFIGURATION;
    pkt->wValue = cfg; /* the chosen config */
    pkt->wIndex = 0x0000; /* offset */
    pkt->wLength = 0x0000; /* data length */
}

/**
 * Create an USB CLEAR_FEATURE control package addressed
 * to an endpoint.
 */
static void create_clear_feature_pkt(
    struct usb_ctrlrequest* pkt, alt_u8 cfg, alt_u16 ep_addr)
{
    /**
     * D7:   0:host to device / 1:device to host
     * D6-5: 0: standard, 1: class, 2: vendor, 3: reserved
     */
    pkt->bRequestType = 0x02;
    pkt->bRequest = CTRL_CLEAR_FEATURE;
    pkt->wValue = cfg; /* the feature selector */
    /*
     * wIndex: bit7=0 for Control or OUT EP, bit7=1 IN EP
     *         bit0-3: EP number
     * (this is excactly what bEndpointAddress contains)
     */
    pkt->wIndex = ep_addr;
    pkt->wLength = 0x0000; /* data length */
}

/**
 * Create an USB GET_STATUS control package addressed
 * to an endpoint.
 */
static void create_get_status_pkt(struct usb_ctrlrequest* pkt, alt_u16 ep_addr)
{
    /**
     * D7:   0:host to device / 1:device to host
     * D6-5: 0: standard, 1: class, 2: vendor, 3: reserved
     */
    pkt->bRequestType = 0x82;
    pkt->bRequest = CTRL_GET_STATUS;
    pkt->wValue = 0x0000;
    /*
     * wIndex: bit7=0 for Control or OUT EP, bit7=1 IN EP
     *         bit0-3: EP number
     * (this is excactly what bEndpointAddress contains)
     */
    pkt->wIndex = ep_addr;
    pkt->wLength = 0x0002; /* data length */
}

/**
 * Perform the USB enumeration part SET_ADDRESS
 */
int set_address(struct hcd *hcd, alt_u16 addr)
{
    struct td *setup_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));
    struct usb_ctrlrequest *setup_pkt = malloc(sizeof(struct usb_ctrlrequest));

    if(setup_td == NULL || status_td == NULL || setup_pkt == NULL) {
        alt_printf("error: Out of memory!\n");
        return false;
    }

    dbg("SET_ADDRESS\n");
    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;
    hcd->sie->dev_addr = 0;
    create_setup_td(hcd, setup_td, USB_PID_SETUP);
    create_status_td(hcd, status_td, USB_PID_IN, 0);
    create_addr_pkt(setup_pkt, addr);
    setup_td->data = setup_pkt;
    send_td(hcd, setup_td);
    send_td(hcd, status_td);
    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        dbg("SET_ADDRESS failed.\n");
        return false;
    }

    hcd->sie->dev_addr = addr;
    dbg("device address set\n");

    release_td(setup_td);
    release_td(status_td);

    return true;
}

/**
 * Make a control request without data phase.
 *
 */
bool send_ctrl_req(struct hcd *hcd, struct usb_ctrlrequest *ctrl_req)
{
    struct td *setup_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));

    if(!setup_td || !status_td) {
        alt_printf("error: Out of memory!\n");
        return false;
    }

    dbg("\n *** start control transfer *** \n");

    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;
    hcd->toggle = 0;
    create_setup_td(hcd, setup_td, USB_PID_SETUP);
    hcd->toggle = 1;
    create_status_td(hcd, status_td, USB_PID_IN, 0);
    setup_td->data = (void*)ctrl_req;

    dbg("\n *** send setup td *** \n");
    send_td(hcd, setup_td);
    dbg("\n *** send status td *** \n");
    send_td(hcd, status_td);

    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        release_td(setup_td);
        release_td(status_td);
        dbg("control transfer failed.\n");
        return false;
    }

    release_td(setup_td);
    release_td(status_td);

    return true;
}


bool send_ctrl_req_with_data_in(struct hcd *hcd,
    struct usb_ctrlrequest *ctrl_req, void *data)
{
    struct td *setup_td = malloc(sizeof(struct td));
    struct td *data_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));

    if(!setup_td || !status_td) {
        alt_printf("error: Out of memory!\n");
        return false;
    }

    dbg("\n *** start control transfer *** \n");

    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;

    create_setup_td(hcd, setup_td, USB_PID_SETUP);
    create_data_td(hcd, data_td, USB_PID_IN, ctrl_req->wLength, 0);
    create_status_td(hcd, status_td, USB_PID_OUT, 0);
    setup_td->data = (void*)ctrl_req;
    dbg("\n *** send setup td *** \n");
    hcd->toggle = 0;
    send_td(hcd, setup_td);
    dbg("\n *** send data td *** \n");
    hcd->toggle = 1;
    send_td(hcd, data_td);
    dbg("\n *** send status td *** \n");
    send_td(hcd, status_td);

    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        release_td(setup_td);
        release_td(status_td);
        dbg("control transfer failed.\n");
        return false;
    }

	read_mem(hcd->sie->dev, data_td->ly_base_addr, data, ctrl_req->wLength);

    release_td(setup_td);
    release_td(data_td);
    release_td(status_td);

    return true;
}

/**
 * Parameter data will be the status result.
 */
bool ctrl_get_status(struct hcd *hcd, void *data)
{
    struct usb_ctrlrequest *req = malloc(sizeof(struct usb_ctrlrequest));
    // free is done on release_td
    dbg("\n *** GET_STATUS *** \n");
    create_get_status_pkt(req, bulk_in_ep_addr(hcd->sie->dev));
    return send_ctrl_req_with_data_in(hcd, req, data);
}


bool get_descr(struct hcd *hcd, int desc_type, void *descr, int desc_size)
{
    struct td *setup_td = malloc(sizeof(struct td));
    struct td *data_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));
    struct usb_ctrlrequest *setup_pkt = malloc(sizeof(struct usb_ctrlrequest));

    if(!setup_td || !data_td || !status_td || !setup_pkt) {
        alt_printf("error: Out of memory!\n");
        return false;
    }

    dbg("\n *** GET_DESCRIPTOR *** \n");

    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;

    create_setup_td(hcd, setup_td, USB_PID_SETUP);
    create_data_td(hcd, data_td, USB_PID_IN, desc_size, 0);
    create_status_td(hcd, status_td, USB_PID_OUT, 0);
    create_get_descriptor_pkt(setup_pkt, desc_type, 0, desc_size);
    setup_td->data = (void*)setup_pkt;

    dbg("\n *** send setup td *** \n");
    send_td(hcd, setup_td);
    dbg("\n *** send data td *** \n");
    send_td(hcd, data_td);
    dbg("\n *** send status td *** \n");
    send_td(hcd, status_td);

    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        dbg("GET_DESCRIPTOR failed.\n");
        return false;
    }

    parse_descr(hcd, descr, desc_size, data_td->ly_base_addr);

    release_td(setup_td);
    release_td(data_td);
    release_td(status_td);

    return true;
}

/**
 * Perform the USB enumeration SET_CONFIGURATION part. We use the first
 * configuration as windows would do, most devices expect that.
 */
bool set_configuration(struct hcd *hcd) {

    bool success;
    struct td *setup_td = malloc(sizeof(struct td));
    struct td *data_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));
    struct usb_ctrlrequest *setup_pkt = malloc(sizeof(struct usb_ctrlrequest));
    struct usb_device *dev = hcd->sie->dev;

    if(!setup_td || !data_td || !status_td || !setup_pkt) {
        alt_printf("Out of memory!\n");
        return false;
    }

    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;

    create_setup_td(hcd, setup_td, USB_PID_SETUP);
    create_data_td(hcd, data_td, USB_PID_IN, 0, 0);
    create_status_td(hcd, status_td, USB_PID_OUT, 0);

    create_set_config_pkt(setup_pkt, dev->cfg_descr->bConfigurationValue);
    setup_pkt->wValue = dev->cfg_descr->bConfigurationValue;
    setup_td->data = (void*)setup_pkt;

    dbg("\n SET_CONFIGURATION\n");
    dbg(" *** send setup td *** \n");
    send_td(hcd, setup_td);
    dbg("\n *** send data td *** \n");
    send_td(hcd, data_td);
    dbg("\n *** send status td *** \n");
    send_td(hcd, status_td);

    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        dbg("SET_CONFIGURATION failed (timeout).\n");
        return false;
    }

    release_td(setup_td);
    release_td(data_td);
    release_td(status_td);

    // need to (re)get the configuration descriptor to see if the new
    // configuration is active.
    success = get_descr(hcd, DESCR_TYPE_CFG, dev->cfg_descr,
        sizeof(struct config_descriptor));

    if(!success) {
        alt_printf("err: get config descriptor after set config failed.\n");
        return false;
    }

    if(dev->cfg_descr->bConfigurationValue != 1) {
        alt_printf("err: unconfigured device after SET_CONFIGURATION\n");
        return false;
    }

    return true;
}

/**
 * Retrives the device and configuration descriptors and set them
 * in the device struct
 */
static bool load_descriptors(struct hcd *hcd)
{
    int success;
    int len_cfgdescr = sizeof(struct config_descriptor);
    int len_ifdescr = sizeof(struct interface_descriptor);
    int len_epdescr = sizeof(struct endpoint_descriptor);
    int ep_offset = 0;
    struct usb_device *dev = hcd->sie->dev;

    dbg("\nGET_DEVICE_DESCRIPTOR\n");
    success = get_descr(hcd, DESCR_TYPE_DEV, dev->dev_descr,
        sizeof(struct device_descriptor));

    if(!success) {
        dbg("get device descriptor failed.\n");
        return false;
    }

    dbg("\nGET_CONFIGURATION DESCRIPTOR\n");
    success = get_descr(hcd, DESCR_TYPE_CFG, dev->cfg_descr, len_cfgdescr);

    if(!success) {
        dbg("get configuration descriptor failed.\n");
        return false;
    }

    dbg("Number of Interface descriptors: %x\n", dev->cfg_descr->bNumInterfaces);
    dbg("config descriptor total length: %x\n", dev->cfg_descr->wTotalLength);

    /**
     * Interface Descriptor and Endpoint Descriptors are part of the
     * GET_CONFIGURATION_DESCRIPTOR request the wTotalLength tells us what
     * is the total length of cfg/if/ep-descr together.
     */
    alt_u8 *buf = malloc(dev->cfg_descr->wTotalLength);
    success = get_descr(hcd, DESCR_TYPE_CFG, buf, dev->cfg_descr->wTotalLength);

    // This codes expects there is one if descr and 2 ep descr
    // as most likly will be but it could be different
    if(dev->cfg_descr->bNumInterfaces != 1) {
        alt_printf("Error: more than one interface descriptor found.\n");
        return false;
    }

    memcpy(hcd->sie->dev->if_descr, buf+len_cfgdescr, len_ifdescr);

	if(dev->if_descr->bInterfaceClass != 0x08) {
        alt_printf("Device is not an mass storage device. found 0x%x\n",
            dev->if_descr->bInterfaceClass);
        return false;
    }

    if(dev->if_descr->bInterfaceSubClass != 0x06) {
        alt_printf("Device does'nt support SCSI transparent command set\n");
        return false;
    }

    for(int i=0; i<dev->if_descr->bNumEndpoints; ++i) {
        memcpy(dev->ep_descr[i], buf+len_cfgdescr+len_ifdescr+ep_offset,
            len_epdescr);
        ep_offset += len_epdescr;

        if(dev->ep_descr[i]->bmAttributes != 0x02) {
            alt_printf("Descriptor is not an bulk endpoint descriptor.\n");
            return false;
        }
    }

    free(buf);

    dbg("mass-storage device detected!\n");
    return true;
}

/**
 * Device enumeration:
 *  - assign address to the device
 *  - request: device descriptor, configuration descriptor, interface descriptor
 *    endpoint descriptor, string descriptor 0 (Language ID),
 *    string descriptor 1 (Manufacturer String), string descriptor 2
 *    (Product String), string descriptor 3 (Serial Number)
 *
 * A USB mass-storage device must have all of the following:
 *  - An interface descriptor with the class code = 08h (mass storage).
 *  - A bulk IN endpoint and a bulk OUT endpoint that belong to the
 *    mass-storage interface.
 *  - A serial number stored in a string descriptor.
 */
bool configure_device(struct hcd *hcd)
{
    int success;

    alt_u16 addr = 0x0002;
    success = set_address(hcd, addr);

    if(!success) {
        dbg("set address failed.\n");
        return false;
    }

    success = load_descriptors(hcd);

    if(!success) {
        dbg("get descriptors failed.\n");
        return false;
    }

    success = set_configuration(hcd);

    if(!success) {
        dbg("set configuration failed.\n");
        return false;
    }

    return true;
}

 /*****  USB MSC class specific functions *****/


void bulk_create_status_td(struct hcd *hcd, struct td *td, alt_u8 pid, alt_u8 ep,
    alt_u16 data_len)
{
    create_td(hcd, td, pid, data_len, ep);
    td->next_td_addr = 0x0;
    td->retry_cnt = 0x1B; // bulk
}


/**
 *  Create a bulk only mass storage reset packet.
 *  This request is used to reset the mass storage device and its associated
 *  interface. This class-specific request shall ready the device for the next
 *  CBW. This request must be send via the default pipe.
 */
void bulk_create_reset_pkt(struct usb_ctrlrequest *pkt)
{
    /**
     * D7  : Direction : 0:host to device, 1:device to host
     * D6-5: Type      : 0:standard, 1:class, 2:vendor, 3:reserved
     * D4-0: Recipient : 0:Device, 1:iface, 2:ep, 3:other, 4,3,1 reserved
     */
    pkt->bRequestType = 0x21; // set class, host to device and Recipient: iface
    pkt->bRequest = US_BULK_RESET_REQUEST;
    pkt->wValue = 0x0000;
    pkt->wIndex = 0x0000; /* iface number */
    pkt->wLength = 0x0000; /* data length */
}


/**
 * Use bCBWLUN (see 5.1 Command Block Wrapper (CBW)) to designate which logical
 * unit of the device is the destination of the CBW.  The Get Max LUN device
 * request is used to determine the number of logical units supported by the
 * device. The device shall return one byte of data that contains the maximum
 * LUN supported by the device.
 */
void bulk_create_maxlun_pkt(struct usb_ctrlrequest *pkt)
{
    pkt->bRequestType = 0xA1; // set class, device to host and Recipient: iface
    pkt->bRequest = US_BULK_GET_MAX_LUN;
    pkt->wValue = 0x0000;
    pkt->wIndex = 0x0000; /* iface number */
    pkt->wLength = 0x0001; /* data length */
}


/**
 * Send USB bulk only request
 */
bool bulk_send_ctrl(struct hcd *hcd, int ctrl)
{
    struct td *setup_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));
    struct usb_ctrlrequest *ctrl_pkt = malloc(sizeof(struct usb_ctrlrequest));

    if(setup_td == NULL || status_td == NULL || ctrl_pkt == NULL) {
        alt_printf("error: Out of memory!\n");
        return false;
    }

    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;
    create_setup_td(hcd, setup_td, USB_PID_SETUP);
    setup_td->retry_cnt = 0x1B; // bulk see bios manual 3.5.7
    create_status_td(hcd, status_td, USB_PID_IN, 0);
    status_td->retry_cnt = 0x1B; // bulk

    switch(ctrl) {
        case US_BULK_RESET_REQUEST:
            dbg("\n *** BULK RESET *** \n");
            bulk_create_reset_pkt(ctrl_pkt);
            break;
        case US_BULK_GET_MAX_LUN:
            dbg("\n *** BULK GET MAX LUN *** \n");
            bulk_create_maxlun_pkt(ctrl_pkt);
            break;
        default:
            alt_printf("Unknown control packet identifier\n");
            return false;
    }
    ctrl_pkt->wIndex = hcd->sie->dev->if_descr->bInterfaceNumber;
    setup_td->data = ctrl_pkt;
    send_td(hcd, setup_td);
    send_td(hcd, status_td);
    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        dbg("send bulk ctrl failed.\n");
        return false;
    }

    if(!is_td_ok(hcd, status_td)) {
        dbg_td(status_td, "errornous status td");
        return false;
    }

    release_td(setup_td);
    release_td(status_td);

    return true;
}

/**
 * For Reset Recovery the host shall issue the following order:
 *  1) Bulk-Only Mass Storage Reset
 *  2) Clear Feature HALT to the Bulk-IN  endpoint
 *  3) Clear Feature HALT to the Bulk-Out endpoint
 */
bool bulk_reset_recovery(struct hcd *hcd)
{
    struct usb_device *dev = hcd->sie[0].dev;
    struct usb_ctrlrequest *ctrl_req_in = malloc(sizeof(struct usb_ctrlrequest));
    struct usb_ctrlrequest *ctrl_req_out = malloc(sizeof(struct usb_ctrlrequest));
    int success = bulk_send_ctrl(hcd, US_BULK_RESET_REQUEST);
    if(!success) return false;

    if(!ctrl_req_in || !ctrl_req_out) {
        alt_printf("error: Out of memory!\n");
        return false;
    }

    create_clear_feature_pkt(ctrl_req_in, ENDPOINT_HALT, bulk_in_ep_addr(dev));
    dbg("\n CLEAR_FEATURE bulk IN ep\n");
    dbg("wIndex (wEndpoint): 0x%x\n", ctrl_req_in->wIndex);
    send_ctrl_req(hcd, ctrl_req_in);
    dbg("\n CLEAR_FEATURE bulk OUT ep\n");
    dbg("wIndex (wEndpoint): 0x%x\n", ctrl_req_out->wIndex);
    create_clear_feature_pkt(ctrl_req_out, ENDPOINT_HALT, bulk_out_ep_addr(dev));
    send_ctrl_req(hcd, ctrl_req_out);

    // free is done by release_td!

    return true;
}

void fill_cbw(struct hcd *hcd, struct cbw *cbw, int len_data_phase,
    int len_cdb, int dir)
{
    cbw->Signature = US_BULK_CB_SIGN;
    cbw->Tag = hcd_next_tag(hcd); // a value which associates the cbw with the csw

    cbw->DataTransferLength = len_data_phase; // number of bytes to receive
    cbw->Lun = 0x00;

    if(dir == USB_DIR_IN) {
        cbw->Flags = 0x80;
    } else if (dir == USB_DIR_OUT) {
        cbw->Flags = 0x0;
    } else {
        alt_printf("Invalid direction\n");
        return;
    }

    if(len_cdb > 16) {
        alt_printf("err: CDB max size is 16 bytes.\n");
        return;
    }

    cbw->Length = len_cdb;
    // Initialize to zero.
    for(int i=0; i<len_cdb; ++i) {
        cbw->CDB[i] = 0;
    }
}


/**
 * cbw:  The Command Block Wrapper struct to init
 * cmd:  The SCSI command
 * addr: LBA to operate on
 * blks: Number of blocks to address each block is 512 byte
 */
void fill_cdb(struct cbw *cbw, alt_u8 cmd, alt_u32 addr, alt_u8 blks)
{

	cbw->CDB[0] = cmd; // set scsi command
    // set logical block start address
    cbw->CDB[2] = addr & 0xff000000;
    cbw->CDB[3] = addr & 0x00ff0000;
    cbw->CDB[4] = addr & 0x0000ff00;
    cbw->CDB[5] = addr & 0x000000ff;
    cbw->CDB[8] = blks; // set number of blocks
}


struct td* bulk_fill_data_tds(struct hcd *hcd, void *data, unsigned int data_len,
    alt_u16 dir)
{
    struct td *head, *td, *tail;
    struct usb_device *dev = hcd->sie->dev;
    alt_u16 ep_max_pkt = bulk_out_ep_descr(dev)->wMaxPacketSize;
    int remaining = data_len;
    int len = (remaining > ep_max_pkt) ? ep_max_pkt : remaining;
    int data_offset = 0;

    if(remaining == 0) {
        alt_printf("err: bulk_fill_data_td cant have data length 0\n");
        exit(1);
    }
    head = malloc(sizeof(struct td));

    if(!head) {
        alt_printf("Out of memory!\n");
        exit(1);
    }

    if(dir == USB_DIR_IN) { /* read */
        create_data_td(hcd, head, USB_PID_IN, len, bulk_in_ep_addr(dev));
    } else { /* write */
        create_data_td(hcd, head, USB_PID_OUT, len, bulk_out_ep_addr(dev));
        // Copy data into the td data buffer.
        head->data = malloc(sizeof(alt_u8)*len);
        if(!head->data) {
            free(head);
            alt_printf("error: memory allocation failed\n");
            exit(1);
        }
        memcpy(head->data, data+data_offset, sizeof(alt_u8)*len);
        data_offset += len;
    }

    hcd->toggle = !hcd->toggle;

    remaining -= len;
    head->nxt_td = NULL;
    head->retry_cnt = 0x1B;

    while(remaining) {
        len = (remaining > ep_max_pkt) ? ep_max_pkt : remaining;
        td = malloc(sizeof(struct td));

        if(!td) {
            alt_printf("Out of memory!\n");
            exit(1);
        }

        if(dir == USB_DIR_IN) {
            create_data_td(hcd, td, USB_PID_IN, len, bulk_in_ep_addr(dev));
        } else if (dir == USB_DIR_OUT) {
            create_data_td(hcd, td, USB_PID_OUT, len, bulk_out_ep_addr(dev));
            // Copy data into the td data buffer.
            td->data = malloc(sizeof(alt_u8)*len);
            if(!td->data) {
                free(td);
                alt_printf("error: memory allocation failed\n");
                exit(1);
            }

            memcpy(td->data, data+data_offset, sizeof(alt_u8)*len);
            data_offset += len;
        } else {
            free(td);
            alt_printf("error: Unknown direction.\n");
            exit(1);
        }

        hcd->toggle = !hcd->toggle;
        td->retry_cnt = 0x1B;

        if(head->nxt_td == NULL) {
            head->nxt_td = td;
        } else {
            tail->nxt_td = td;
        }

        tail = td;
        remaining -= len;
    }

    return head;
}


bool is_csw_valid(struct csw *csw, struct cbw *cbw)
{
    dbg_csw(csw);

    if(csw->Signature != US_BULK_CS_SIGN) {
        alt_printf("err: CSW have invalid signature.\n");
        return false;
    }

    if(csw->Tag != cbw->Tag) {
        alt_printf("err: CSW have wrong tag.\n");
        return false;
    }

    switch(csw->Status) {

        case 0x01:
            alt_printf("CSW: Command failed\n");
            return false;
        case 0x02:
            alt_printf("CSW: Phase error.\n");
            return false;

    }

    return true;
}

/**
 * Send a Command Block Wrapper this could contain data which should be written
 * or it can retreive data from the usb msc device. After the CBW is send, the
 * functions gets the corresponding CSW from the device.
 */
bool bulk_send_cbw(struct hcd *hcd, struct cbw *cbw, struct csw *csw, void *buf)
{
    struct td *data_tds, *data_td_head;
    struct td *command_td = malloc(sizeof(struct td));
    struct td *status_td = malloc(sizeof(struct td));

    if(command_td == NULL || status_td == NULL) {
        dbg("\nOut of memory!\n");
        return false;
    }

    hcd->next_td_addr = hcd->td_base_addr;
    hcd->next_buf_addr = hcd->buf_base_addr;
    dbg("\n *** send CBW *** \n");

    hcd->toggle = 0;
    create_command_td(hcd, command_td, USB_PID_OUT);

    hcd->toggle = !hcd->toggle;

    dbg_cbw(cbw);

    if(cbw->CDB[0] == WRITE_10) {
        // we want to send data
        data_tds = bulk_fill_data_tds(hcd, buf, cbw->DataTransferLength,
            USB_DIR_OUT);
    } else if(cbw->CDB[0] == READ_10) {
        // we want to receive data
        data_tds = bulk_fill_data_tds(hcd, NULL, cbw->DataTransferLength,
            USB_DIR_IN);
    } else {
        alt_printf("Function supports SCSI READ(10) and WRITE(10)\n");
        return false;
    }

    data_td_head = data_tds;
    hcd->toggle = 1; // status phase has always DATA1 toggle
    bulk_create_status_td(hcd, status_td, USB_PID_IN,
        bulk_in_ep_addr(hcd->sie->dev), sizeof(struct csw));

    if(data_tds == NULL) {
        return false;
    }

    command_td->data = cbw;
    status_td->data = csw;

    dbg("\n *** send command td *** \n");
    send_td(hcd, command_td);

    while(data_tds != NULL) {
        dbg("\n *** send data td *** \n");
        send_td(hcd, data_tds);
        data_tds = data_tds->nxt_td;
    }

    dbg("\n *** send status td *** \n");
    send_td(hcd, status_td);

    husb_set_current_td(hcd->sie, hcd->td_base_addr);

    if(!wait_tddone(hcd)) {
        dbg("send CBW failed.\n");
        return false;
    }

    if(!is_td_ok(hcd, status_td)) {
        dbg_td(status_td, "errornous status td");
        //return false;
    }

    // get the csw from the device
    dbg("read csw: 0x%x\n", status_td->ly_base_addr);
    read_mem(hcd->sie->dev, status_td->ly_base_addr, csw, sizeof(struct csw));

    if(!is_csw_valid(csw, cbw)) {

        // Not all data recived so we have still to use the previous tag.
        if(csw->Residue > 0 && hcd->tag > 0) {
            hcd->tag -= 1;
        }

        return false;
    }

    dbg("CSW is ok!\n");

    // get the data from the device if IN
    if(cbw->CDB[0] == READ_10) {
        read_mem(hcd->sie->dev, data_td_head->ly_base_addr, buf,
            cbw->DataTransferLength);
    }

    release_td(command_td);
    release_td(data_td_head);
    release_td(status_td);

    return true;
}


