/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * @author Matthias Meß
 *
 * The DHT11 is a basic, low-cost digital temperature and humidity sensor.
 * It uses a capacitive humidity sensor and a thermistor to measure the
 * surrounding air, and spits out a digital signal on the data pin
 * (no analog input pins needed).
 * It requires careful timing to grab data. The only real downside of this
 * sensor is you can only get new data from it once every 2 seconds.
 *
 * This implementation is based on the VHDL implementation which does the
 * hard work. To use the module the enable signal must be asserted and then
 * have to poll for the ready signal.
 */

#ifndef DHT11_H
#define DHT11_H

#include "helper.h"
#include <stdbool.h>

struct dht11_data
{
    alt_u16 temperature;
    alt_u16 humidity;
    bool checksum_err;
    bool timeout_err;
};


/**
 * One read cycle needs about 23 ms and a read should not occur
 * more often then once per 2 seconds.
 */
void dht11_read(struct dht11_data *data);

#endif
