/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef C67200_BULK_H
#define C67200_BULK_H

#include "c67200-hcd.h"
#include "helper.h"
#include <stdbool.h>

#define USB_DIR_OUT         0     /* to device */
#define USB_DIR_IN          0x80  /* to host */

#define USB_PID_OUT         0x1
#define USB_PID_IN          0x9
#define USB_PID_SETUP       0xd
#define USB_PID_SOF         0x5
#define USB_PID_PREAMBLE    0xc
#define USB_PID_ACK         0x2
#define USB_PID_NAK         0xa
#define USB_PID_STALL       0xe
#define USB_PID_DATA0       0x3
#define USB_PID_DATA1       0xb

#define ENDPOINT_ENABLED  0
#define ENDPOINT_DISABLED 1
#define USB_ENDPOINT_MAXP_MASK	0x07ff

struct pipe {
    int num;
    int type;
    int direction;
};

/**
 * struct usb_ctrlrequest - SETUP data for a USB device control request
 * @bRequestType: matches the USB bmRequestType field
 * @bRequest: matches the USB bRequest field
 * @wValue: matches the USB wValue field (le16 byte order)
 * @wIndex: matches the USB wIndex field (le16 byte order)
 * @wLength: matches the USB wLength field (le16 byte order)
 *
 * This structure is used to send control requests to a USB device.  It matches
 * the different fields of the USB 2.0 Spec section 9.3, table 9-2.  See the
 * USB spec for a fuller description of the different fields, and what they are
 * used for.
 */
struct usb_ctrlrequest
{
	alt_u8 bRequestType;
	alt_u8 bRequest;
	alt_16 wValue;
	alt_16 wIndex;
	alt_16 wLength;
};

#define US_BULK_CS_WRAP_LEN	13
#define US_BULK_CS_SIGN		0x53425355      /* csw signature */
#define US_BULK_CB_SIGN		0x43425355      /* csb signature */
#define US_BULK_STAT_OK		0
#define US_BULK_STAT_FAIL	1
#define US_BULK_STAT_PHASE	2
/* bulk-only class specific requests */
#define US_BULK_RESET_REQUEST   0xff
#define US_BULK_GET_MAX_LUN     0xfe

/* command block wrapper 
 *
 * CDB structure: 1b cmd, 1b flags, 4b LBA, 1b group, 2b length, 1b ctrl
 **/
struct cbw {
	alt_u32 Signature;		/* contains 'USBC' */
	alt_u32	Tag;			/* unique per command id */
	alt_u32 DataTransferLength;	/* size of data */
	alt_u8	Flags;			/* direction in bit 0 */
	alt_u8  Lun;			/* LUN normally 0 */
	alt_u8  Length;			/* length of the CDB in bytes */
	alt_u8  CDB[16];		/* max command */
} __attribute__ ((packed));

/* command status wrapper */
struct csw {
    alt_u32 Signature;
    alt_u32 Tag;
    alt_u32 Residue;
    alt_u8 Status;
} __attribute__ ((packed));

/** Control Transfer Related **/
bool configure_device(struct hcd *hcd);
bool ctrl_get_status(struct hcd *hcd, void *data);
/** Bulk Transfer Related **/
void fill_cbw(struct hcd *hcd, struct cbw *cbw, int len_data_phase,
    int len_cdb, int dir);
void fill_cdb(struct cbw *cbw, alt_u8 cmd, alt_u32 addr, alt_u8 len);
bool bulk_reset_recovery(struct hcd *hcd);
bool bulk_send_ctrl(struct hcd *hcd, int ctrl);
bool bulk_send_cbw(struct hcd *hcd, struct cbw *cbw, struct csw *csw, void *buf);

#endif

