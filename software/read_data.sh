#!/bin/bash

dd if=/dev/sdb bs=512 count=1 | xxd -c 4 usb.img | gawk '{
    print "Temperature: " strtonum("0x"substr($2,0,2)) "." strtonum("0x"substr($2, 3, 2)) "C"
    print "Humidity   : " strtonum("0x"substr($3, 0, 2)) "." strtonum("0x"substr($3, 3, 2)) "\%"
}'

