/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * @author Matthias Mess
 *
 * DHT11 Driver for Altera FPGA platform.
 * The driver uses the Avalon Interface IP-Core
 * which are written to create a communication channel to the DHT11 Controller
 * VHDL Entity.
 *
 * REG32_AVALON_INTERFACE_0_BASE is 32 bit memory mapped io which contains:
 *
 * read:
 * bit 31-16: temperature
 * bit 15-0:  humidity
 *
 * REG8_AVALON_INTERFACE_0_BASE is 8 bit memory mapped io which contains:
 *
 * read:
 * bit 0: ready (1 means ready)
 * bit 1: timeout_err (1 means error)
 * bit 2: checksum_err (1 means error)
 * write:
 * bit 7 is the enable signal.
 */

#include <io.h>
#include <system.h>
#include <unistd.h>
#include "dht11.h"

#define BIT_EN            0x8
#define MASK_HUMIDITY     0x0000FFFF
#define GET_TEMPERATURE(buf) buf >> 16
#define MASK_RDY          0x1
#define MASK_TIMEOUT_ERR  0x2
#define MASK_CHECKSUM_ERR 0x4

/* one MS in US to feed usleep */
#define MS 1000


void dbg_data(struct dht11_data *data)
{
    dbg("Temperature: %x\n", data->temperature);
    dbg("Humidity: %x\n", data->humidity);
    dbg("Checksum: %x\n", data->checksum_err);
    dbg("Timeout: %x\n", data->timeout_err);
}

/**
 * Asserting EN let the sensor generating new data.
 */
void en()
{
    IOWR_8DIRECT(REG8_AVALON_INTERFACE_0_BASE, 0, BIT_EN);
    usleep(3);
    IOWR_8DIRECT(REG8_AVALON_INTERFACE_0_BASE, 0, 0);
}

/**
 * If the sensor is rdy with getting data rdy will be 1.
 * This takes around 30ms.
 */
bool is_rdy()
{
    alt_u8 buf;
    buf = IORD_8DIRECT(REG8_AVALON_INTERFACE_0_BASE, 0);
    return buf & MASK_RDY;
}

/**
 * One read cycle needs about 30 ms and a read should not occur
 * more often then once per 2 seconds.
 */
void dht11_read(struct dht11_data *data)
{
    int tries = 5;
    alt_u32 buf;
    alt_u8 buf8;

    en();
    usleep(30*MS);

    while(!is_rdy()) {
        if(tries-- == 0) {
            dbg("err: waiting for dht11 rdy timed out.\n");
            data->timeout_err = true;
            dbg_data(data);
            return;
        }
        usleep(1000*MS);
    }

    buf = IORD_32DIRECT(REG32_AVALON_INTERFACE_0_BASE, 0);
    data->humidity = buf & MASK_HUMIDITY;
    data->temperature = GET_TEMPERATURE(buf);
    buf8 = IORD_8DIRECT(REG8_AVALON_INTERFACE_0_BASE, 0);
    data->checksum_err = buf8 & MASK_CHECKSUM_ERR;
    data->timeout_err = buf8 & MASK_TIMEOUT_ERR;
    dbg_data(data);
}

