/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CY7C67200_H_
#define CY7C67200_H_

#include <system.h>
#include <io.h>
#include "alt_types.h"

#define PIPE_CONTROL 0
#define PIPE_BULK 1

/* ---------------------------------------------------------------------
 * Cypress C67200 register definitions
 */
/* Hardware Revision Register */
#define HW_REV_REG      0xC004

/* General USB registers */
/* ===================== */
/* USB Control Register */
#define USB_CTL_REG(x)      ((x) ? 0xC0AA : 0xC08A)
#define LOW_SPEED_PORT(x)   ((x) ? 0x0800 : 0x0400)
#define HOST_MODE       0x0200
#define PORT_RES_EN(x)      ((x) ? 0x0100 : 0x0080)
#define SOF_EOP_EN(x)       ((x) ? 0x0002 : 0x0001)
/* USB status register - Notice it has different content in hcd/udc mode */
#define USB_STAT_REG(x)     ((x) ? 0xC0B0 : 0xC090)
#define EP0_IRQ_FLG      0x0001
#define EP1_IRQ_FLG      0x0002
#define EP2_IRQ_FLG      0x0004
#define EP3_IRQ_FLG      0x0008
#define EP4_IRQ_FLG      0x0010
#define EP5_IRQ_FLG      0x0020
#define EP6_IRQ_FLG      0x0040
#define EP7_IRQ_FLG      0x0080
#define RESET_IRQ_FLG    0x0100
#define SOF_EOP_IRQ_FLG  0x0200
#define ID_IRQ_FLG       0x4000
#define VBUS_IRQ_FLG     0x8000
/* USB Host only registers */
/* ======================= */
/* Host n Control Register */
#define HOST_CTL_REG(x) ((x) ? 0xC0A0 : 0xC080)
#define PREAMBLE_EN     0x0080  /* Preamble enable */
#define SEQ_SEL         0x0040  /* Data Toggle Sequence Bit Select */
#define ARM_EN          0x0001  /* Arm operation */
/* Host n Interrupt Enable Register */
#define HOST_IRQ_EN_REG(x)  ((x) ? 0xC0AC : 0xC08C)
#define SOF_EOP_IRQ_EN      0x0200  /* SOF/EOP Interrupt Enable  */
#define SOF_EOP_TMOUT_IRQ_EN    0x0800  /* SOF/EOP Timeout Interrupt Enable  */
#define ID_IRQ_EN       0x4000  /* ID interrupt enable */
#define VBUS_IRQ_EN     0x8000  /* VBUS interrupt enable */
#define DONE_IRQ_EN     0x0001  /* Done Interrupt Enable  */
/* USB status register */
#define HOST_STAT_MASK      0x02FD
#define PORT_CONNECT_CHANGE(x)  ((x) ? 0x0020 : 0x0010)
#define PORT_SE0_STATUS(x)  ((x) ? 0x0008 : 0x0004)
/* Host Frame Register */
#define HOST_FRAME_REG(x)   ((x) ? 0xC0B6 : 0xC096)
#define HOST_FRAME_MASK     0x07FF
/* USB Peripheral only registers */
/* ============================= */
/* Device n Port Sel reg */
#define DEVICE_N_PORT_SEL(x)    ((x) ? 0xC0A4 : 0xC084)
/* Device n Interrupt Enable Register */
#define DEVICE_N_IRQ_EN_REG(x)  ((x) ? 0xC0AC : 0xC08C)
#define DEVICE_N_ENDPOINT_N_CTL_REG(dev, ep)    ((dev)  \
                         ? (0x0280 + (ep << 4)) \
                         : (0x0200 + (ep << 4)))
#define DEVICE_N_ENDPOINT_N_STAT_REG(dev, ep)   ((dev)  \
                         ? (0x0286 + (ep << 4)) \
                         : (0x0206 + (ep << 4)))
#define DEVICE_N_ADDRESS(dev)   ((dev) ? (0xC0AE) : (0xC08E))
/* HPI registers */
/* ============= */
/* HPI Status register */
#define SOFEOP_FLG(x)   (1 << ((x) ? 12 : 10))
#define SIEMSG_FLG(x)   (1 << (4 + (x)))
#define RESET_FLG(x)    ((x) ? 0x0200 : 0x0002)
#define DONE_FLG(x)     (1 << (2 + (x)))
#define RESUME_FLG(x)   (1 << (6 + (x)))
#define MBX_OUT_FLG     0x0001  /* Message out available */
#define MBX_IN_FLG      0x0100
#define ID_FLG          0x4000
#define VBUS_FLG        0x8000
/* Interrupt routing register */
#define HPI_IRQ_ROUTING_REG 0x0142
#define HPI_SWAP_ENABLE(x)  ((x) ? 0x0100 : 0x0001)
#define RESET_TO_HPI_ENABLE(x)  ((x) ? 0x0200 : 0x0002)
#define DONE_TO_HPI_ENABLE(x)   ((x) ? 0x0008 : 0x0004)
#define RESUME_TO_HPI_ENABLE(x) ((x) ? 0x0080 : 0x0040)
#define SOFEOP_TO_HPI_EN(x) ((x) ? 0x2000 : 0x0800)
#define SOFEOP_TO_CPU_EN(x) ((x) ? 0x1000 : 0x0400)
#define ID_TO_HPI_ENABLE    0x4000
#define VBUS_TO_HPI_ENABLE  0x8000
/* SIE msg registers */
#define SIEMSG_REG(x)       ((x) ? 0x0148 : 0x0144)
#define HUSB_TDListDone     0x1000
#define SUSB_EP0_MSG        0x0001
#define SUSB_EP1_MSG        0x0002
#define SUSB_EP2_MSG        0x0004
#define SUSB_EP3_MSG        0x0008
#define SUSB_EP4_MSG        0x0010
#define SUSB_EP5_MSG        0x0020
#define SUSB_EP6_MSG        0x0040
#define SUSB_EP7_MSG        0x0080
#define SUSB_RST_MSG        0x0100
#define SUSB_SOF_MSG        0x0200
#define SUSB_CFG_MSG        0x0400
#define SUSB_SUS_MSG        0x0800
#define SUSB_ID_MSG         0x4000
#define SUSB_VBUS_MSG       0x8000
/* BIOS interrupt routines */
#define SUSBx_RECEIVE_INT(x)    ((x) ? 97 : 81)
#define SUSBx_SEND_INT(x)   ((x) ? 96 : 80)
#define SUSBx_DEV_DESC_VEC(x)   ((x) ? 0x00D4 : 0x00B4)
#define SUSBx_CONF_DESC_VEC(x)  ((x) ? 0x00D6 : 0x00B6)
#define SUSBx_STRING_DESC_VEC(x) ((x) ? 0x00D8 : 0x00B8)
#define CY_HCD_BUF_ADDR     0x500   /* Base address for host */
#define SIE_TD_SIZE         0x200   /* size of the td list */
#define SIE_TD_BUF_SIZE     0x400   /* size of the data buffer */
#define SIE_TD_OFFSET(host) ((host) ? (SIE_TD_SIZE+SIE_TD_BUF_SIZE) : 0)
#define SIE_BUF_OFFSET(host)    (SIE_TD_OFFSET(host) + SIE_TD_SIZE)
/* Base address of HCD + 2 x TD_SIZE + 2 x TD_BUF_SIZE */
#define CY_UDC_REQ_HEADER_BASE  0x1100
/* 8- byte request headers for IN/OUT transfers */
#define CY_UDC_REQ_HEADER_SIZE  8
#define CY_UDC_Rine CY_UDC_REQ_BUFFER_ADDR(ep_num)  (CY_UDC_REQ_BUFFER_BASE + \
                     ((ep_num) * CY_UDC_REQ_BUFFER_SIZE))
/* ---------------------------------------------------------------------
 * Driver data structures
 */
struct usb_device;

/* SIE configuration */
#define C67X00_SIE_UNUSED   0
#define C67X00_SIE_HOST     1
#define C67X00_SIE_PERIPHERAL_A 2   /* peripheral on A port */
#define C67X00_SIE_PERIPHERAL_B 3   /* peripheral on B port */
#define C67X00_SIE1_UNUSED          (C67X00_SIE_UNUSED      << 0)
#define C67X00_SIE1_HOST            (C67X00_SIE_HOST        << 0)
#define C67X00_SIE1_PERIPHERAL_A    (C67X00_SIE_PERIPHERAL_A    << 0)
#define C67X00_SIE1_PERIPHERAL_B    (C67X00_SIE_PERIPHERAL_B    << 0)
#define C67X00_SIE2_UNUSED          (C67X00_SIE_UNUSED      << 4)
#define C67X00_SIE2_HOST            (C67X00_SIE_HOST        << 4)
#define C67X00_SIE2_PERIPHERAL_A    (C67X00_SIE_PERIPHERAL_A    << 4)
#define C67X00_SIE2_PERIPHERAL_B    (C67X00_SIE_PERIPHERAL_B    << 4)

#define C67X00_SIES  2
#define C67X00_PORTS 2

#define sie_config(config, n)  (((config)>>(4*(n)))&0x3)

struct platform_data {
    int sie_config;         /* SIEs config (C67200_SIEx_*) */
    unsigned long hpi_regstep;  /* Step between HPI registers  */
};

/**
 * struct sie - Common data associated with a SIE
 * @hcd: subdriver dependent data
 * @irq: subdriver dependent irq handler, set NULL when not used
 * @dev: link to common driver structure
 * @num: SIE number on chip, starting from 0
 * @mode: SIE mode (host/peripheral/otg/not used)
 */
struct sie {
    alt_u16 dev_addr; /* the addr of usb device as assigned with SET_ADDRESS */
    struct hcd *hcd;
    void (*irq) (struct sie *sie, alt_u16 int_status, alt_u16 msg);
    /* Read only: */
    struct usb_device *dev;
    int num;
    int mode;
};

/**
 * struct lcp
 */
struct lcp {
    volatile alt_u8 msg_received;
    alt_u16 last_msg;
};

/*
 * struct hpi
 */
struct hpi {
    int base;
    int regstep;
    struct lcp lcp;
};


struct hcd {
    struct sie *sie;
    alt_u16 td_base_addr;
    alt_u16 buf_base_addr;
    alt_u16 next_td_addr;
    alt_u16 next_buf_addr;
    alt_u8 endpoint_disable;
    int toggle;
    int tag;
};


/**
 * struct usb_device - Common data associated with a c67x00 instance
 * @hpi: hpi addresses
 * @sie: array of sie's on this chip
 * @pdev: platform device of instance
 * @pdata: configuration provided by the platform
 */
struct usb_device {
    int devnum;
    struct hpi hpi;
    struct sie sie[C67X00_SIES];
    struct platform_device *pdev;
    struct platform_data *pdata;
    alt_u8 portnum;
    /* descriptors */
    struct device_descriptor *dev_descr;
    struct config_descriptor *cfg_descr;
    struct endpoint_descriptor *ep_descr[2];
    struct interface_descriptor *if_descr;
};

/**
 *  USB Standard Descriptors
 *
 * Note all descriptors are declared '__attribute__((packed))' so that:
 *
 * [a] they never get padded, either internally (USB spec writers
 *     probably handled that) or externally;
 *
 * [b] so that accessing bigger-than-a-bytes fields will never
 *     generate bus errors on any platform, even when the location of
 *     its descriptor inside a bundle isn't "naturally aligned"
 */
struct device_descriptor {
    alt_u8  bLength;
    alt_u8  bDescriptorType;
    alt_u16 bcdUSB;
    alt_u8  bDeviceClass;
    alt_u8  bDeviceSubClass;
    alt_u8  bDeviceProtocol;
    alt_u8  bMaxPacketSize0;
    alt_u16 idVendor;
    alt_u16 idProduct;
    alt_u16 bcdDevice;
    alt_u8  iManufacturer;
    alt_u8  iProduct;
    alt_u8  iSerialNumber;
    alt_u8  bNumConfigurations;
} __attribute__ ((packed));

struct endpoint_descriptor {
    alt_u8  bLength;
    alt_u8  bDescriptorType;
    alt_u8  bEndpointAddress;
    alt_u8  bmAttributes;
    alt_u16 wMaxPacketSize;
    alt_u8  bInterval;
} __attribute__ ((packed));

struct interface_descriptor {
    alt_u8  bLength;
    alt_u8  bDescriptorType;
    alt_u8  bInterfaceNumber;
    alt_u8  bAlternateSetting;
    alt_u8  bNumEndpoints;
    alt_u8  bInterfaceClass;
    alt_u8  bInterfaceSubClass;
    alt_u8  bInterfaceProtocol;
    alt_u8  iInterface;
} __attribute__ ((packed));

struct config_descriptor {
    alt_u8  bLength;
    alt_u8  bDescriptorType;
    alt_u16 wTotalLength;
    alt_u8  bNumInterfaces;
    alt_u8  bConfigurationValue;
    alt_u8  iConfiguration;
    alt_u8  bmAttributes;
    alt_u8  bMaxPower;
} __attribute__((packed));

struct string_descriptor {
    alt_u8 bLength;
    alt_u8 bDescriptorType;
    alt_u16 bString[1]; /* UTF-16LE encoded */
} __attribute__ ((packed));


/* ---------------------------------------------------------------------
 * Low level interface functions
 */

/* Host Port Interface (HPI) functions */
alt_u16 hpi_status(struct usb_device *dev);
void hpi_reg_init(struct usb_device *dev);
void hpi_enable_sofeop(struct sie *sie);
void hpi_disable_sofeop(struct sie *sie);

alt_u16 hpi_read_word(struct usb_device *dev, alt_u16 reg);

/* General functions */
alt_u16 fetch_siemsg(struct usb_device *dev, int sie_num);
alt_u16 get_usb_ctl(struct sie *sie);
void usb_clear_status(struct sie *sie, alt_u16 bits);
alt_u16 usb_get_status(struct sie *sie);
void write_mem(struct usb_device *dev, alt_u16 addr, void *data, int len);
void read_mem(struct usb_device *dev, alt_u16 addr, void *data, int len);

/* Host specific functions */
void set_husb_eot(struct usb_device *dev, alt_u16 value);
void husb_reset(struct sie *sie, int port);
void husb_set_current_td(struct sie *sie, alt_u16 addr);
alt_u16 husb_get_current_td(struct sie *sie);
alt_u16 husb_get_frame(struct sie *sie);
void husb_init_host_port(struct sie *sie);
void husb_reset_port(struct sie *sie, int port);

/*----------------------------------------------------------------------
 * High level functions
 **/
int hcd_next_tag(struct hcd *hcd);
void init_device(struct usb_device *dev);
void release_device(struct usb_device *dev);
int reset_device(struct usb_device *dev);
void init_sie(struct sie *sie, struct usb_device *dev, int sie_num);
void wait_for_connect(struct sie *sie);
void init(struct usb_device *dev, int sie_num, int port);

#endif /* _CY7C67200_H */

