/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * @author Matthias Mess
 * Measurement System Application.
 * The application gets measurement data using a dht11 temp/hum sensor.
 * The data will be saved using a usb mass storage device.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "helper.h"
#include "scsi.h"
#include "msys.h"
#include "dht11.h"

#define STATUS_HALT 1
#define BLOCK_LEN 512
#define CDB_LEN 10

// One Second in US to feed usleep (no sleep available).
#define SEC 1000000

/**
 * Dump a memory block.
 */
void dbg_block(alt_u8 *buf, int len)
{
    for(int i=0; i<len; ++i) {
        alt_printf("%x ", buf[i]);
        if(!((i+1) % 16)) alt_printf("\n");
    }
}


bool msys_read_data(struct sie *sie)
{
    int success;
    struct cbw *cbw = malloc(sizeof(struct cbw));
    struct csw *csw = malloc(sizeof(struct csw));
    // allocate mem for one block of data we want to read
    int block_len = 512;
    int blks = 1;
    alt_u8 *buf = malloc(blks*block_len*sizeof(alt_u8));
    int len_data = blks*block_len;
    dbg("\n   **** READ DATA **** \n");
    memset(buf, 0, len_data);

    fill_cbw(sie->hcd, cbw, len_data, CDB_LEN, USB_DIR_IN);
    alt_u32 lba_addr = 0x0;
    fill_cdb(cbw, READ_10, lba_addr, blks);
    success = bulk_send_cbw(sie->hcd, cbw, csw, buf);

    if(!success) {
        alt_printf("send CBW failed\n");
        return false;
    }

    alt_printf("\n");
    for(int i=0; i<len_data; ++i) {
        alt_printf("%x ", *buf++);
        if(((i+1) % 16) == 0) dbg("\n");
    }

    return true;
}


bool usb_write_data(struct sie *sie, struct dht11_data *data, alt_u8 *blk,
    alt_u32 lba_addr, int lba_offset)
{
    bool success;
    struct cbw *cbw = malloc(sizeof(struct cbw));
    struct csw *csw = malloc(sizeof(struct csw));

    if(!cbw || !csw) {
        alt_printf("Out of Memory.\n");
        exit(1);
    }

    blk[lba_offset] = data->temperature >> 8;
    blk[lba_offset+1] = data->temperature;
    blk[lba_offset+2] = data->humidity >> 8;
    blk[lba_offset+3] = data->humidity;

    fill_cbw(sie->hcd, cbw, BLOCK_LEN, CDB_LEN, USB_DIR_OUT);
    fill_cdb(cbw, WRITE_10, lba_addr, 1);
    dbg("\n   **** WRITE DHT11 DATA **** \n");
    dbg_block(blk, BLOCK_LEN);
    success = bulk_send_cbw(sie->hcd, cbw, csw, blk);

    // free csw/cbw is done by bulk_send_cbw!

    return success;
}


bool msys_write_data(struct sie *sie)
{
    struct dht11_data data;
    alt_u8 *blk = malloc(BLOCK_LEN*sizeof(alt_u8));
    alt_u16 status = 0;
    int lba_addr = 0, lba_offset = 0, max_tries = 3;
    memset(blk, 0, BLOCK_LEN);

    while((lba_offset+4) != BLOCK_LEN) {

        if(max_tries == 0) {
            alt_printf("Tried 3 times, give up..\n");
            return false;
        }

        dht11_read(&data);

        if(data.timeout_err || data.checksum_err) {
            alt_printf("Reading sensor data failed.\n");
            max_tries -= 1;
            usleep(2*SEC); /* DHT11 can do a measurement every 2 seconds. */
            continue;
        }

        if(usb_write_data(sie, &data, blk, lba_addr, lba_offset)) {
            max_tries = 3;
            lba_offset += 4;
            dbg("LBA OFFSET: %x\n", lba_offset);
            usleep(5*SEC);
            continue;
        }

        max_tries -= 1;

        if(!ctrl_get_status(sie->hcd, &status)) {
            alt_printf("err: GET_STATUS failed\n");
            usleep(2*SEC);
            continue;
        }

        if(status == STATUS_HALT) {
            dbg("\n Recover from HALT\n");
            bulk_reset_recovery(sie->hcd);
            usleep(2*SEC);
        }
    }

    free(blk);

    return lba_offset == BLOCK_LEN;
}

