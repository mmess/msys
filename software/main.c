/*
 * msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
 * Copyright (C) 2018  Matthias Meß
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include "sys/alt_stdio.h"
#include "helper.h"
#include "c67200-hcd.h"
#include "c67200-bulk.h"
#include "msys.h"

int main()
{
    int i, ret;
    struct usb_device dev;

    alt_printf("\n--------START---------\n");
    init_device(&dev);
    dev.pdata->sie_config = C67X00_SIE1_HOST | C67X00_SIE2_UNUSED;

    for (i = 0; i < C67X00_SIES; i++)
        init_sie(&dev.sie[i], &dev, i);

    init(&dev, 0, 0);
    wait_for_connect(&dev.sie[0]);
    ret = configure_device(dev.sie[0].hcd);

    if(!ret) {
        alt_printf("Configure device failed.\n");
        return 1;
    }

    ret = bulk_send_ctrl(dev.sie[0].hcd, US_BULK_RESET_REQUEST);

    if(!ret) {
        alt_printf("Bulk reset failed.\n");
        return 1;
    }

    ret = msys_write_data(&dev.sie[0]);

    if(!ret) {
        alt_printf("Bulk write failed.\n");
        return 1;
    }

    alt_printf("\n-------- END ---------\n");
    return 0;
}

