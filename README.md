Within the scope of this project is the VHDL implementation of
a DHT11 Sensor controller and Avalon MM 8 bit and 32 bit register to offer
a interface to the measurement data from the software part.

The software folder contains implementation of host controller / bulk drivers
for the cy7c6200 USB Controller from cypress. And code to read/write the
registers of the DHT11 interface.