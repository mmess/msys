-- msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
-- Copyright (C) 2018  Matthias Meß
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

entity reg32_avalon_interface is
    port (
        clk, reset_n: in std_logic;
        read, write: in std_logic;
        writedata: in std_logic_vector(31 downto 0);
        readdata: out std_logic_vector(31 downto 0);
        q_export_write: in std_logic_vector(31 downto 0)
    );
end reg32_avalon_interface;

architecture structure of reg32_avalon_interface is

    signal from_reg, to_reg : std_logic_vector(31 downto 0);

    component reg32
        port (
            clk, reset_n: in std_logic;
            d: in std_logic_vector(31 downto 0);
            q: out std_logic_vector(31 downto 0)
        );
    end component;
begin

    to_reg <= q_export_write;
    reg_instance: reg32
        port map (
            clk,
            reset_n,
            to_reg,
            from_reg
        );

    readdata <= from_reg;

end structure;
