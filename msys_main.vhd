library ieee;
use ieee.std_logic_1164.all;

entity msys_main is
    port (
        clk: in std_logic;
        rst: in std_logic;
        dht11_d: inout std_logic;
        temperature, humidity: out std_logic_vector(15 downto 0);
        timeout_err, checksum_err: out std_logic;
        debug: out     std_logic_vector(3 downto 0);
        usb_DATA: inout std_logic_vector(15 downto 0) := (others => 'X');
        usb_ADDR: out   std_logic_vector(1 downto 0);
        usb_RD_N: out   std_logic;
        usb_WR_N: out   std_logic;
        usb_CS_N: out   std_logic;
        usb_RST_N: out  std_logic;
        usb_INT: in     std_logic := 'X'
    );
end msys_main;

architecture structure of msys_main is

    signal s_temperature: std_logic_vector(15 downto 0);
    signal s_humidity: std_logic_vector(15 downto 0);
    signal s_debug: std_logic_vector(3 downto 0);
    signal s_checksum_err, s_timeout_err, s_rdy, s_en: std_logic := '0';
    signal s_reg32_writedata: std_logic_vector(31 downto 0):= (others => '0');
    signal s_reg8_readdata: std_logic_vector(7 downto 0) := (others => '0');
    signal s_reg8_writedata: std_logic_vector(7 downto 0) := (others => '0');
    
    component msys is
        port (
            clk_50: in std_logic := 'X';
            reset_n: in std_logic := 'X';
            reg32_conduit_end_writedata: in std_logic_vector(31 downto 0);
            reg8_conduit_end_readdata: out std_logic_vector(7 downto 0);
            reg8_conduit_end_writedata: in std_logic_vector(7 downto 0)  := (others => '0');
            usb_conduit_end_DATA: inout std_logic_vector(15 downto 0) := (others => 'X');
            usb_conduit_end_ADDR: out std_logic_vector(1 downto 0);
            usb_conduit_end_RD_N: out std_logic;
            usb_conduit_end_WR_N: out std_logic;
            usb_conduit_end_CS_N: out std_logic;
            usb_conduit_end_RST_N: out std_logic;
            usb_conduit_end_INT: in std_logic := 'X'
        );
    end component msys;

    component dht11 is
    port(
        clk: in std_logic;
        rst: in std_logic := '1';
        d: inout std_logic := 'X';
        en: in std_logic;
        rdy: out std_logic;
        temperature, humidity: out std_logic_vector(15 downto 0);
        timeout_err, checksum_err: out std_logic;
        debug: out std_logic_vector(3 downto 0)
    );
    end component dht11;

begin

    s_en <= s_reg8_readdata(7);

    u0: component msys
    port map (
        clk_50 => clk,
        reset_n => rst,
        reg32_conduit_end_writedata => s_reg32_writedata, -- reg32.q_export_write
        reg8_conduit_end_readdata => s_reg8_readdata,     -- reg8.q_export_read
        reg8_conduit_end_writedata => s_reg8_writedata,   -- reg8.q_export_write

        usb_conduit_end_DATA => usb_DATA,
        usb_conduit_end_ADDR => usb_ADDR,
        usb_conduit_end_RD_N => usb_RD_N,
        usb_conduit_end_WR_N => usb_WR_N,
        usb_conduit_end_CS_N => usb_CS_N,
        usb_conduit_end_RST_N => usb_RST_N,
        usb_conduit_end_INT => usb_INT
    );

    u1: component dht11
    port map (
        clk => clk,
        rst => rst,
        d => dht11_d,
        en => s_en,
        rdy => s_rdy,
        temperature => s_temperature,
        humidity => s_humidity,
        timeout_err => s_timeout_err,
        checksum_err => s_checksum_err,
        debug => s_debug
    );

    humidity <= s_humidity;
    temperature <= s_temperature;    
    checksum_err <= s_checksum_err;
    timeout_err <= s_timeout_err;
    debug <= s_debug;
    
    s_reg32_writedata(31 downto 16) <= s_temperature;
    s_reg32_writedata(15 downto 0) <= s_humidity;
    s_reg8_writedata(0) <= s_rdy;-- set dht11.RDY bit
    s_reg8_writedata(1) <= s_timeout_err;
    s_reg8_writedata(2) <= s_checksum_err;
    s_reg8_writedata(7 downto 3) <= "00000";

end structure;
