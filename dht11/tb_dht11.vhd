-- msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
-- Copyright (C) 2018  Matthias Meß
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

entity tb_dht11 is
end entity;

architecture testbench of tb_dht11 is
    component dht11 is
        port(
            clk : in std_logic;
            rst : in std_logic := '1';
            d : inout std_logic;
            en : in std_logic;
            rdy: out std_logic := '0';
            temperature, humidity: out std_logic_vector(15 downto 0);
            timeout_err, checksum_err : out std_logic;
            debug : out std_logic_vector(3 downto 0)
        );
    end component;

    signal s_clk, s_rst : std_logic := '0';
    signal s_d, s_en, s_rdy, s_timeout_err, s_checksum_err : std_logic;
    signal s_temperature, s_humidity : std_ulogic_vector(15 downto 0);
begin

    dut: dht11
    port map(
        clk => s_clk,
        rst => s_rst,
        d => s_d,
        en => s_en,
        rdy => s_rdy,
        temperature => s_temperature,
        humidity => s_humidity,
        timeout_err => s_timeout_err,
        checksum_err => s_checksum_err
    );
 
    s_rst <= '1';
 
    clkgen: process(s_clk)
    begin -- simulate 50Mhz clock for proper timing
        s_clk <= (not s_clk) after 10 ns;
    end  process;
    
    datastimuli_d : process
    begin
        s_d <= 'Z';
        -- init (is only working if we wait only 40us instead of 18ms in state low_18ms
        -- waiting 18ms takes to long in simulation)
        -- so to make this working the dht11 entity variable COUNTER_FOR_18MS must be adjusted
        wait for 80 us;
        s_d <= '0';
        wait for 54 us; -- response_1 send 0 for 54us
        s_d <= '1';
        wait for 80 us;-- response_2 send d 1 for 80us
        s_d <= '0';
        wait for 54 us; -- data wait
        -- length of sending 1 determines if 0 or 1
        s_d <= '1';
        wait for 70 us; -- bit 39 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 38 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 37 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 36 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 35 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 34 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 33 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 32 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 31 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 30 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 29 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 28 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 27 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 26 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 25 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 24 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 23 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 22 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 21 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 20 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 19 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 18 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 17 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 16 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 15 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 14 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 13 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 12 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 11 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 10 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 9 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 8 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 7 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 6 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 5 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 4 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 3 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 70 us; -- bit 2 is 1 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 24 us; -- bit 1 is 0 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
        
        s_d <= '0';
        wait for 54 us; -- data wait 
        s_d <= '1';
        wait for 24 us; -- bit 0 is 0 (data read)
        s_d <= '0';
        wait for 5 us; -- data store
    end process;
end testbench;
