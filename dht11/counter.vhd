-- msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
-- Copyright (C) 2018  Matthias Meß
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;

entity counter is
generic (MAX_COUNTER_VALUE: integer := 4);
port (
    clk, rst, stop : in std_logic;
    current_count : out integer
);
end entity;

architecture behavior of counter is
begin

    process(clk, rst, stop)
        variable i : integer := 0;
    begin
        if rst = '1' then
            i := 0;
        else
            if rising_edge(clk) then
                if i /= MAX_COUNTER_VALUE or stop /= '1' then
                    i := i + 1;
                end if;
            end if;
        end if;
        current_count <= i;
    end process;

end architecture;
