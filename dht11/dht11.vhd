-- msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
-- Copyright (C) 2018  Matthias Meß
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dht11 is
    port(
        clk : in std_logic;
        rst : in std_logic := '1';
        d : inout std_logic;
        en : in std_logic;
        rdy: out std_logic := '0';
        temperature, humidity: out std_logic_vector(15 downto 0);
        timeout_err, checksum_err : out std_logic;
        debug : out std_logic_vector(3 downto 0)
    );
end entity;

architecture behavior of dht11 is

    type states is (
        init, low_18ms, high_40us, high_40us_rdy, response_1, response_2,
        data_wait, data_read, data_store, data_out, data_counter_reset,
        low_18ms_rdy, response_1_rdy, response_2_rdy, reset_counter,
        checksum_error, data_wait_rdy, timeout1, timeout2, timeout3, timeout4,
        data_read_error, check
    );
    constant US_1 : integer := 50;  -- one micro second
    -- 18ms is 900000 but we need to wait more then 18ms
    constant COUNTER_FOR_18MS : integer := 1000000; --for simulation put US_1 * 40;
    constant COUNTER_FOR_22US : integer := US_1 * 22;
    constant COUNTER_FOR_26US : integer := US_1 * 26;
    constant COUNTER_FOR_40US : integer := US_1 * 40;
    constant COUNTER_FOR_52US : integer := US_1 * 52;
    constant COUNTER_FOR_56US : integer := US_1 * 56;
    constant COUNTER_FOR_68US : integer := US_1 * 68;
    constant COUNTER_FOR_72US : integer := US_1 * 72;
    constant COUNTER_FOR_78US : integer := US_1 * 78;
    constant COUNTER_FOR_82US : integer := US_1 * 82;
    
    signal current_state, next_state: states;
    signal s_stop, s_rst_counter, sipo_in, sipo_en, sipo_rst: std_logic := '0';
    signal s_current_count: integer := 0;
    signal sipo_out: std_logic_vector(39 downto 0);
    signal s_sipo_cnt: integer range 0 to 40;
    
    
    component counter is
        generic(max_counter_value : integer := 0);
        port(
            clk, rst, stop : in std_logic;
            current_count : out integer
        );
    end component;
    
    component sipo is
    	port( 
            clk, rst, en, nxt_bit : in  std_logic;
            cnt: out integer range 0 to 40;
            q : out std_logic_vector(39 downto 0)
        );
    end component;
begin

    component_sipo: sipo
    port map(
        clk => clk,
        rst => sipo_rst,
        en => sipo_en,
        nxt_bit => sipo_in,
        cnt => s_sipo_cnt,
        q => sipo_out
    );

    component_counter: counter
    generic map(max_counter_value => COUNTER_FOR_18MS)
    port map(
        clk => clk,
        rst => s_rst_counter,
        stop => s_stop,
        current_count => s_current_count
    );
    
    state_register: process(clk, rst, next_state, en)
    begin
        if rst = '0' or en = '1' then
            current_state <= init;
        else
            if rising_edge(clk) then
                current_state <= next_state;
            end if;
        end if;
    end process state_register;


    state_decoder: process(current_state, d, s_current_count, s_sipo_cnt)
    begin
        -- assign default values
        s_rst_counter <= '0';
        s_stop <= '0';
        sipo_in <= '0';
        sipo_rst <= '0';
        sipo_en <= '0';
        
        case current_state is
        
            when init =>
                sipo_rst <= '1';
                s_rst_counter <= '1';
                next_state <= low_18ms;

            when low_18ms =>
                -- requesting data consists of keeping the bus 18ms low and..
                next_state <= low_18ms;
                if s_current_count = COUNTER_FOR_18MS then
                    next_state <= low_18ms_rdy;
                end if;
            when low_18ms_rdy =>
                s_rst_counter <= '1';
                next_state <= high_40us;
            when high_40us =>
                next_state <= high_40us;
                -- .. then 40us high
                if s_current_count = COUNTER_FOR_40US then
                    next_state <= high_40us_rdy;
                end if;
            when high_40us_rdy =>
                s_rst_counter <= '1';
                next_state <= response_1;
                
            when response_1 =>
                -- part 1 of response is ~54us low.
                next_state <= response_1;
                    
                if d = '0' and s_current_count > COUNTER_FOR_40US then
                    next_state <= response_1_rdy;
                elsif s_current_count > COUNTER_FOR_56US then
                    next_state <= timeout1;
                end if;
                
            when response_1_rdy =>
                s_rst_counter <= '1';
                next_state <= response_2;
                
            when response_2 =>
                next_state <= response_2;
                -- Part 2 of the respone is ~80us high.
                if d = '1' and s_current_count > COUNTER_FOR_78US then
                    next_state <= response_2_rdy;
                elsif s_current_count > COUNTER_FOR_82US then
                    next_state <= timeout2;
                end if;
            when response_2_rdy =>
                s_rst_counter <= '1';
                next_state <= data_wait;
                
            when data_wait =>
            -- Each bit sent is a follow of ~54uS Low in the bus and ~24uS to
            -- 70uS High depending on the value of the bit.
            -- Bit '0' : ~54uS Low and ~24uS High
            -- Bit '1' : ~54uS Low and ~70uS High
                next_state <= data_wait;
                
                if d = '0' and s_current_count > COUNTER_FOR_52US
                    and s_current_count < COUNTER_FOR_56US
                then
                    next_state <= data_wait_rdy;
                elsif s_current_count > COUNTER_FOR_56US then
                    next_state <= timeout3;
                end if;
            when data_wait_rdy =>
                next_state <= data_wait_rdy;
                s_rst_counter <= '1';
                if d = '1' then
                    next_state <= data_read;
                end if;
            when data_read =>
                next_state <= data_read;
                if d = '0' then
                    s_stop <= '1';
                    next_state <= data_store;
                elsif s_current_count > COUNTER_FOR_78US then
                    next_state <= timeout4;
                end if;

            when data_store =>
                s_stop <= '1';
                next_state <= data_store;
                
                if s_current_count > COUNTER_FOR_22US and
                    s_current_count < COUNTER_FOR_26US
                then
                    sipo_in <= '0';
                    sipo_en <= '1';
                    if s_sipo_cnt = 39 then
                        next_state <= data_out;
                    else
                        next_state <= response_2_rdy;
                    end if;
                elsif s_current_count > COUNTER_FOR_68US and
                    s_current_count < COUNTER_FOR_78US
                then
                    sipo_in <= '1';
                    sipo_en <= '1';
                    if s_sipo_cnt = 39 then
                        next_state <= data_out;
                    else
                        next_state <= response_2_rdy;
                    end if;
                else
                    next_state <= data_read_error;
                end if;

            when data_out =>
                next_state <= data_out;

            when timeout1 =>
                next_state <= timeout1;
            when timeout2 =>
                next_state <= timeout2;
            when timeout3 =>
                next_state <= timeout3;
            when timeout4 =>
                next_state <= timeout4;
            when check =>
                next_state <= check;
            when others =>
                next_state <= init;
        end case;
    end process state_decoder;


    output_decoder: process(current_state, sipo_out, en)
    begin
        humidity <= (others => '0');
        temperature <= (others => '0');
        timeout_err <= '0';
        checksum_err <= '0';
        d <= 'Z';
        debug <= "0100";
        rdy <= '0';
        
        case current_state is
        
            when init =>
                d <= '1';
                debug <= "0011";
            when low_18ms =>
                -- keep d 18ms low until we switch in next state and wait for
                -- dht11 response
                d <= '0';
            when low_18ms_rdy | high_40us =>
                -- keep d 40us low.
                d <= '1';
            when response_1 =>
                debug <= "0011";
            when response_2 =>
                debug <= "0100";
            when data_wait =>
                debug <= "0101";
            when data_read =>
                debug <= "0110";
            when data_store =>
                debug <= "0111";
            when data_out =>
                debug <= "1001";
                humidity <= sipo_out(39 downto 24);
                temperature <= sipo_out(23 downto 8);
                rdy <= '1';
                
                if unsigned(sipo_out(39 downto 32)) + unsigned(sipo_out(31 downto 24))
                    + unsigned(sipo_out(23 downto 16)) + unsigned(sipo_out(15 downto 8))
                    /= unsigned(sipo_out(7 downto 0)) then
                   checksum_err <= '1';
               end if;
            when data_read_error =>
                debug <= "1111";
                rdy <= '1';
            when timeout1 => -- response_1
                timeout_err <= '1';
                rdy <= '1';
                debug <= "1000";
            when timeout2 => --response_2
                debug <= "1100";
                timeout_err <= '1';
                rdy <= '1';
            when timeout3 => -- data_wait
                debug <= "1101";
                timeout_err <= '1';
                rdy <= '1';
            when timeout4 => -- data_read
                debug <= "1110";
                timeout_err <= '1';
                rdy <= '1';
            when check =>
                debug <= "0011";
                timeout_err <= '1';
            when others =>
                debug <= "0000";
        end case;
    end process output_decoder;

end architecture;
