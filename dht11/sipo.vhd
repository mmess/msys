-- msys - DHT11 measurement system with CY7C67200 USB Driver for DE2-115 FPGA
-- Copyright (C) 2018  Matthias Meß
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

entity sipo is
	port( 
		clk, rst, en, nxt_bit : in  std_logic;
        cnt : out integer range 0 to 40;
		q : out  std_logic_vector(39 downto 0)
	);
end sipo;

architecture behavioral of sipo is
    signal s_data : std_logic_vector(39 downto 0);
    signal s_cnt : integer range 0 to 40;
begin

    process(clk)
    variable data_tmp:  std_logic_vector(39 downto 0);
    begin
    if rising_edge(clk) then
        if rst = '1' then
            s_data <= (others => '0');
            s_cnt <= 0;
            q <= (others => '0');
        else
            if s_cnt < 39 and en = '1' then
                -- left shift
                s_data <= s_data(38 downto 0) & nxt_bit;
                s_cnt <= s_cnt + 1;
            elsif en = '1' then
                s_cnt <= 0;
                data_tmp := s_data(38 downto 0) & nxt_bit;
                q <= data_tmp(39 downto 0);
             end if;
        end if;
        cnt <= s_cnt;
    end if;
    end process;

end behavioral;
