
module msys (
	clk_50,
	reset_n,
	reg32_conduit_end_writedata,
	reg8_conduit_end_readdata,
	reg8_conduit_end_writedata,
	usb_conduit_end_DATA,
	usb_conduit_end_ADDR,
	usb_conduit_end_RD_N,
	usb_conduit_end_WR_N,
	usb_conduit_end_CS_N,
	usb_conduit_end_RST_N,
	usb_conduit_end_INT);	

	input		clk_50;
	input		reset_n;
	input	[31:0]	reg32_conduit_end_writedata;
	output	[7:0]	reg8_conduit_end_readdata;
	input	[7:0]	reg8_conduit_end_writedata;
	inout	[15:0]	usb_conduit_end_DATA;
	output	[1:0]	usb_conduit_end_ADDR;
	output		usb_conduit_end_RD_N;
	output		usb_conduit_end_WR_N;
	output		usb_conduit_end_CS_N;
	output		usb_conduit_end_RST_N;
	input		usb_conduit_end_INT;
endmodule
