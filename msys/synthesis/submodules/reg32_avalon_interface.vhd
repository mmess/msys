library ieee;
use ieee.std_logic_1164.all;

entity reg32_avalon_interface is
    port (
        clk, reset_n: in std_logic;
        read, write: in std_logic;
        writedata: in std_logic_vector(31 downto 0);
        readdata: out std_logic_vector(31 downto 0);
        q_export_write: in std_logic_vector(31 downto 0)
    );
end reg32_avalon_interface;

architecture structure of reg32_avalon_interface is

    signal from_reg, to_reg : std_logic_vector(31 downto 0);

    component reg32
        port (
            clk, reset_n: in std_logic;
            d: in std_logic_vector(31 downto 0);
            q: out std_logic_vector(31 downto 0)
        );
    end component;
begin

    to_reg <= q_export_write;
    reg_instance: reg32
        port map (
            clk,
            reset_n,
            to_reg,
            from_reg
        );

    readdata <= from_reg;

end structure;
