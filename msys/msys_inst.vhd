	component msys is
		port (
			clk_50                      : in    std_logic                     := 'X';             -- clk
			reset_n                     : in    std_logic                     := 'X';             -- reset_n
			reg32_conduit_end_writedata : in    std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			reg8_conduit_end_readdata   : out   std_logic_vector(7 downto 0);                     -- readdata
			reg8_conduit_end_writedata  : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- writedata
			usb_conduit_end_DATA        : inout std_logic_vector(15 downto 0) := (others => 'X'); -- DATA
			usb_conduit_end_ADDR        : out   std_logic_vector(1 downto 0);                     -- ADDR
			usb_conduit_end_RD_N        : out   std_logic;                                        -- RD_N
			usb_conduit_end_WR_N        : out   std_logic;                                        -- WR_N
			usb_conduit_end_CS_N        : out   std_logic;                                        -- CS_N
			usb_conduit_end_RST_N       : out   std_logic;                                        -- RST_N
			usb_conduit_end_INT         : in    std_logic                     := 'X'              -- INT
		);
	end component msys;

	u0 : component msys
		port map (
			clk_50                      => CONNECTED_TO_clk_50,                      --       clk_50_clk_in.clk
			reset_n                     => CONNECTED_TO_reset_n,                     -- clk_50_clk_in_reset.reset_n
			reg32_conduit_end_writedata => CONNECTED_TO_reg32_conduit_end_writedata, --   reg32_conduit_end.writedata
			reg8_conduit_end_readdata   => CONNECTED_TO_reg8_conduit_end_readdata,   --    reg8_conduit_end.readdata
			reg8_conduit_end_writedata  => CONNECTED_TO_reg8_conduit_end_writedata,  --                    .writedata
			usb_conduit_end_DATA        => CONNECTED_TO_usb_conduit_end_DATA,        --     usb_conduit_end.DATA
			usb_conduit_end_ADDR        => CONNECTED_TO_usb_conduit_end_ADDR,        --                    .ADDR
			usb_conduit_end_RD_N        => CONNECTED_TO_usb_conduit_end_RD_N,        --                    .RD_N
			usb_conduit_end_WR_N        => CONNECTED_TO_usb_conduit_end_WR_N,        --                    .WR_N
			usb_conduit_end_CS_N        => CONNECTED_TO_usb_conduit_end_CS_N,        --                    .CS_N
			usb_conduit_end_RST_N       => CONNECTED_TO_usb_conduit_end_RST_N,       --                    .RST_N
			usb_conduit_end_INT         => CONNECTED_TO_usb_conduit_end_INT          --                    .INT
		);

