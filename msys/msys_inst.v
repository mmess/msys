	msys u0 (
		.clk_50                      (<connected-to-clk_50>),                      //       clk_50_clk_in.clk
		.reset_n                     (<connected-to-reset_n>),                     // clk_50_clk_in_reset.reset_n
		.reg32_conduit_end_writedata (<connected-to-reg32_conduit_end_writedata>), //   reg32_conduit_end.writedata
		.reg8_conduit_end_readdata   (<connected-to-reg8_conduit_end_readdata>),   //    reg8_conduit_end.readdata
		.reg8_conduit_end_writedata  (<connected-to-reg8_conduit_end_writedata>),  //                    .writedata
		.usb_conduit_end_DATA        (<connected-to-usb_conduit_end_DATA>),        //     usb_conduit_end.DATA
		.usb_conduit_end_ADDR        (<connected-to-usb_conduit_end_ADDR>),        //                    .ADDR
		.usb_conduit_end_RD_N        (<connected-to-usb_conduit_end_RD_N>),        //                    .RD_N
		.usb_conduit_end_WR_N        (<connected-to-usb_conduit_end_WR_N>),        //                    .WR_N
		.usb_conduit_end_CS_N        (<connected-to-usb_conduit_end_CS_N>),        //                    .CS_N
		.usb_conduit_end_RST_N       (<connected-to-usb_conduit_end_RST_N>),       //                    .RST_N
		.usb_conduit_end_INT         (<connected-to-usb_conduit_end_INT>)          //                    .INT
	);

